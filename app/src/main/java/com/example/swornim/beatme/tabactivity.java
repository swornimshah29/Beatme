package com.example.swornim.beatme;

import android.Manifest;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContentResolverCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.swornim.beatme.Fragment.*;
import com.example.swornim.beatme.datastructure.DatabaseDetails;
import com.example.swornim.beatme.datastructure.Payment;
import com.example.swornim.musicnap.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class tabactivity extends AppCompatActivity implements RewardedVideoAdListener {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPageAdapter viewPageAdapter;
    private ListView dialogListview;
    private ListView loanListView;
    private MenuItem menuItem;
    private NavigationView navigationView;
    private FragmentTransaction fragmentTransaction;
    private RewardedVideoAd mRewardedVideoAd;
    private InterstitialAd mInterstitialAd;
    private boolean watching;

    private ImageView addPhoto;
    private LinearLayout adsPhotoLayout;
    private ImageView user_notification;
    private TextView new_ads_text;

    private Runnable adsRunnableTimer;
    private Handler adsHandlerTimer;
    private Thread adsThread;

    private Handler handler = new Handler();
    private Runnable runnable;
    private ProgressBar notificationProgressBar;
    private ProgressBar loanProgressBar;

    private static String BALANCE_USSD_CODE = "901";
    private TextView loan_custom_amount = null;//intital null
    private TextView loan_amount = null;//intital null
    private static Boolean ACCESSIBILITY_ALLOWED = false;
    private List<DatabaseDetails> adapterSource=new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainwrappertoolbar);


        addPhoto = (ImageView) findViewById(R.id.addPhoto);
        adsPhotoLayout = (LinearLayout) findViewById(R.id.adsPhotoLayout);
        new_ads_text = (TextView) findViewById(R.id.new_ads_text);
        user_notification = (ImageView) findViewById(R.id.user_notification);
        addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPickIntent = new Intent(Intent.ACTION_PICK);
                photoPickIntent.setType("image/*");
                startActivityForResult(photoPickIntent, 0);
            }
        });
        LocalBroadcastManager.getInstance(getApplicationContext()).
                registerReceiver(USSDBroadcastReceiver, new IntentFilter("balanceIntent"));

        callFirebase();

        user_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                LocalBroadcastManager.getInstance(getApplicationContext()).
//                        registerReceiver(NotificationBroadcastReceiver, new IntentFilter("notificationIntent"));
//
//                Dialog dialog = new Dialog(tabactivity.this, android.R.style.Theme_Wallpaper_NoTitleBar_Fullscreen);
//                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                    @Override
//                    public void onDismiss(DialogInterface dialogInterface) {
//                        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(NotificationBroadcastReceiver);
//
//                    }
//                });
//                dialog.setTitle("Sub questions");
//                dialog.setContentView(R.layout.notificationlistview);
//                dialog.setCancelable(true);
//                dialog.show();
//
//
//                dialogListview = (ListView) dialog.findViewById(R.id.notificationListViewId);
//                notificationProgressBar = (ProgressBar) dialog.findViewById(R.id.notificationProgressBar);
//                notificationLoad();
//




//                Gson gson=new Gson();
//
//                Events.Notification.Nbet nbet1=new Events().new Notification().new Nbet();
//                Events.Notification.Nbet nbet2=new Events().new Notification().new Nbet();
//                Events.Notification.Nbet nbet=new Events().new Notification().new Nbet();
//                List<Events.Notification.Nbet> nbetLIst=new ArrayList<>();
//
//                nbet1.setTeamName1("asdasdasd");
//                nbet1.setUser2("asdasdasd");
//                nbet1.setUniqueId2("asdasdasd");
//
//                nbet2.setTeamName1("asdasdasd");
//                nbet2.setUser2("asdasdasd");
//                nbet2.setUniqueId2("asdasdasd");
//
//
//                nbetLIst.add(nbet1);
//                nbetLIst.add(nbet2);
//
//                nbet.setNbets(nbetLIst);
//                String jsonString=gson.toJson(nbet);
//                Log.i("mytag","json format"+jsonString);
//

            }
        });

        adsPhotoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mRewardedVideoAd.isLoaded())
                    mRewardedVideoAd.show();
            }

        });

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPageAdapter = new ViewPageAdapter(getSupportFragmentManager());
        viewPageAdapter.addFragments(new EventFragment());
        viewPageAdapter.addFragments(new Homefragment());
        viewPageAdapter.addFragments(new Yesterday());
        viewPageAdapter.addFragments(new Gallery());

        viewPager.setAdapter(viewPageAdapter);
        tabLayout.setupWithViewPager(viewPager);

        adsHandlerTimer = new Handler();
        adsRunnableTimer = new Runnable() {
            @Override
            public void run() {
                Log.i("mytag", "showing ads after 10 seconds " );

                MobileAds.initialize(getApplicationContext(), "ca-app-pub-3940256099942544~3347511713");
                mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(getApplicationContext());
                mRewardedVideoAd.setRewardedVideoAdListener(tabactivity.this);
                mRewardedVideoAd.loadAd("ca-app-pub-3940256099942544/5224354917",
                        new AdRequest.Builder().build());
//                if(watching)
//                    adsHandlerTimer.removeCallbacks(adsRunnableTimer);
//                else
//                adsHandlerTimer.postDelayed(adsRunnableTimer,10000);
                //if condition
                //remove the handler by: handler.removeCallbacks(runnable);

            }
        };
        adsHandlerTimer.postDelayed(adsRunnableTimer, 100);

        //testing

        DatabaseDetails bets=new DatabaseDetails();
        bets.setAmount("200");
        bets.setLeftTeamName("manu");
        bets.setRightTeamName("liverpool");
        bets.setLastSeen("1231231");
        bets.setScore("2-1");
        bets.setEventId("2dsaasasdas00");
        bets.setDieHardFan("barcelona");
        bets.setOpponentNumber("3243432");
        bets.setUserNumber("324343asdasda2");
        bets.setOpponentsim("ntc");
        FirebaseDatabase.getInstance().getReference().child("users/phonenumber1/bets").push().setValue(bets);
        updateContacts();


    }


    private class Status implements Serializable{
        private String status;
        private String wasteId;
        private String sync;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getWasteId() {
            return wasteId;
        }

        public void setWasteId(String wasteId) {
            this.wasteId = wasteId;
        }

        public String getSync() {
            return sync;
        }

        public void setSync(String sync) {
            this.sync = sync;
        }
    }
    private void callFirebase(){

        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                "https://us-central1-shivaya-6c9f9.cloudfunctions.net/newDriverRequest",
//                "http://digitate-convention.000webhostapp.com/beatme/EventsSource.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.i("mytag","response "+response);

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("mytag", "something went wrong during the fetch firebase"+error);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                Status status=new Status();
                status.setStatus("completed");
                status.setWasteId("7");
                status.setSync("false");//firebase response not came about update


                map.put("request",new Gson().toJson(status));

                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> header=new HashMap<>();
                header.put("Content-Type","application/json");
                return header;
            }
        };
        requestQueue.add(stringRequest);
    }


    private void updateContacts() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Cursor cursor=null;

                String[] projections=
                        {
                                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                                ContactsContract.CommonDataKinds.Phone.NUMBER,
                                ContactsContract.CommonDataKinds.Phone._ID,


                        };

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    cursor = getApplicationContext().
                            getContentResolver().query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,null,null);
                }else{
                    cursor= ContentResolverCompat.
                            query(getApplicationContext().getContentResolver(),ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,null,null,null,null);
                }

                if(cursor!=null){
                    while(cursor.moveToNext()) {

                        DatabaseDetails each=new DatabaseDetails();
                        each.setFriendName(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
                        each.setHasApp("no");//this will be changed by the server
                        String filteredNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        if (filteredNumber != null) {
                            filteredNumber = filteredNumber.replaceAll("[-=+()/!~|*&^#@$:'{}_abcdefghijklmnopqrstuvwxyz;%`.,]", "");

                            if (filteredNumber.length() > 10) {
                                //remove the first n characters
                                filteredNumber = filteredNumber.substring(filteredNumber.length() - 10);
                                each.setFriendNumber(filteredNumber);
                                adapterSource.add(each);
                            }
                        }
                    }

                    /*filter the duplicates objects*/
                    for(int i=0;i<adapterSource.size();i++){
                        for(int j=0;j<adapterSource.size();j++){
                            if(i!=j) {//dont check urself
                                if (adapterSource.get(i).getFriendNumber().equals(adapterSource.get(j).getFriendNumber())) {
                                    adapterSource.remove(j);
                                }
                            }
                        }
                        Log.i("tags","phonenumber "+adapterSource.get(i).getFriendNumber());
                    }

                    Map<String,List<DatabaseDetails>> friendsNode=new HashMap<>();
                    friendsNode.put("friends",adapterSource);
                    FirebaseDatabase.getInstance().getReference().child("friends").setValue(friendsNode).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Log.i("tags","synced completed just now");
                        }
                    });


                    //send to the php server
                    Gson gson=new Gson();
                    final String jsondata=gson.toJson(friendsNode);
                    Log.i("tags","jsondata "+jsondata);

                    RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());

                    StringRequest stringRequest=new StringRequest(Request.Method.POST,
                            "http://192.168.1.111/betme/friends.php",
//                "http://digitate-convention.000webhostapp.com/beatme/EventsSource.php",
                            new com.android.volley.Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.i("tags","server update the contacts" +response);

//                                    mResult=response.toString();
//                                    Log.i("mytag",mResult);
//                                    new MSharedPreferences(mContext).setSharedPref("fetchJsonData",mResult);
//                                    Intent intent=new Intent("myEventName");
//                                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

                                }
                            }, new com.android.volley.Response.ErrorListener() {


                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.i("mytag", "something went wrong during the fetch process"+error);
                        }
                    }){
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String,String> map=new HashMap<>();
                            map.put("fetchJsonData","fetch");
                            map.put("jsondata",jsondata);

                            return map;
                        }

                    };
//                    requestQueue.add(stringRequest);



                }

            }
        }).start();

    }



    private void notificationLoad() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.i("mytag", "Running thread");

                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                StringRequest stringRequest = new StringRequest(Request.Method.POST,
//                "http://digitate-convention.000webhostapp.com/beatme/Competion.php",
                        "http://192.168.1.104/beatme/UserNotification.php",
                        new com.android.volley.Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                String mResult = response;
                                Intent intent = new Intent("notificationIntent");
                                intent.putExtra("MyNbet", mResult);
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                                handler.removeCallbacks(runnable);

                            }
                        }, new com.android.volley.Response.ErrorListener() {


                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Log.i("mytag", "Resending the data");
                        handler.postDelayed(runnable, 1500);
                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> map = new HashMap<>();
                        map.put("uniqueId", new MSharedPreferences(getApplicationContext()).getSharedPref("uniqueUserId"));
                        map.put("Nbet", "yup");

                        return map;
                    }


                };
                requestQueue.add(stringRequest);

            }
        });
    }


    public static boolean isFragmentInBackstack(final FragmentManager fragmentManager, final String fragmentTagName) {
        for (int entry = 0; entry < fragmentManager.getBackStackEntryCount(); entry++) {
            if (fragmentTagName.equals(fragmentManager.getBackStackEntryAt(entry).getName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == RESULT_OK) {
            Uri targetUri = data.getData();

            String[] projection = {MediaStore.Images.Media.DATA,
                    MediaStore.Images.Media.DISPLAY_NAME,
                    MediaStore.Images.Media.DATE_ADDED
            };
            Cursor cursor = managedQuery(targetUri, projection, null, null, null);
            if (cursor != null) {
                int column_index = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                String path = cursor.getString(column_index);
                String imageName = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME));

                Log.i("mytag", path);//save this path for offline photourl in database
                //TODO save this path in database_android

                Log.i("mytag", imageName);
                Log.i("mytag", targetUri.toString());

                Intent intent = new Intent(tabactivity.this, UserGifts.class);
                intent.putExtra("url", targetUri.toString());
                intent.putExtra("imageName", imageName);
                startActivity(intent);

//                ImageView imageView = (ImageView) findViewById(R.id.photoSelectedId);

//                Glide.with(getApplicationContext()).load(targetUri) //or use path
//                        .centerCrop()
//                        .crossFade()
//                        .into(imageView);


            }
        }
    }

    public class ViewPageAdapter extends FragmentPagerAdapter {
        ArrayList<Fragment> fragments = new ArrayList<>();

        public void addFragments(Fragment fragments) {
            this.fragments.add(fragments);
        }


        public ViewPageAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {

            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return "Event";
                case 1:
                    return "Home";
                case 2:
                    return "Yesterday";
                case 3:
                    return "Gallery";
                case 4:
                    return "Camera";

            }
            return null;

        }


    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i("mytag", "oncreatemenu  called");//save this path for offline photourl in database

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.actiobarmenus, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_bar_menus_loan:

             //   LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(LoanPaymentBroadcastreceiver, new IntentFilter("loanPaymentIntent"));
                MinorDetails messageObject = new MinorDetails();
                messageObject.setFunctionCalledForOperation("Loans");
                new ServerSideTask(getApplication(), messageObject).execute();


                Dialog dialog = new Dialog(tabactivity.this, android.R.style.Theme_Wallpaper_NoTitleBar_Fullscreen);
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
               //         LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(LoanPaymentBroadcastreceiver);

                    }
                });
                dialog.setTitle("Payment@Loans");
                dialog.setContentView(R.layout.loan);
                dialog.setCancelable(true);
                dialog.show();

                loanListView = (ListView) dialog.findViewById(R.id.loanListview);
                loanProgressBar = (ProgressBar) dialog.findViewById(R.id.loanProgressbar);
                loan_amount = (TextView) dialog.findViewById(R.id.loan_amount);

                break;


            case R.id.paymentActionBar:
                startActivity(new Intent(tabactivity.this, PaymentProcess.class));
//
            default:

        }
        return false;

    }



//    private class PaymentAdapter extends ArrayAdapter<Events.Notification.Loan> {
//        public PaymentAdapter(Context context) {
//            super(context, R.layout.loan_custom, loans);
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            View mView = convertView;
//            if (convertView == null) {
//                mView = LayoutInflater.from(getContext()).inflate(R.layout.loan_custom, parent, false);
//            }
//            Button payNow = (Button) mView.findViewById(R.id.loan_custom_pay);
//            loan_custom_amount = (TextView) mView.findViewById(R.id.loan_custom_amount);
//            TextView userName = (TextView) mView.findViewById(R.id.loan_custom_username);
//            loan_custom_amount.setText(loans.get(position).getAmount());
//            userName.setText(loans.get(position).getUserName());
//            payNow.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    //all the payment process
//                    /*
//
//                   pay the amount :
//                     if success USSD OR User got message => update the database as clear loan
//
//                    * */
//
//
//                    new Thread(new Runnable() {
//                        @Override
//                        public void run() {
//                            AccessibilityManager am = (AccessibilityManager) getApplicationContext()
//                                    .getSystemService(Context.ACCESSIBILITY_SERVICE);
//
//                            List<AccessibilityServiceInfo> enabledServices = am
//                                    .getEnabledAccessibilityServiceList(AccessibilityEvent.TYPES_ALL_MASK);
//                            for (AccessibilityServiceInfo service : enabledServices) {
//                                Log.i("mytag", service.getId());
//
//                                //user level approval for accessibility services
//                                if (service.getId().contains("com.example.swornim.beatme")
//                                        | service.getId().contains("beatme")
//                                        | service.getId().contains("com.example.swornim.beatme/.USSDListener")
//                                        | service.getId().contains("swornim")) {
//                                    //user has already allowed
//                                    ACCESSIBILITY_ALLOWED = true;
//                                    break;
//
//                                } else {
//                                    ACCESSIBILITY_ALLOWED = false;
//                                }
//
//
//                            }
//
//                                if (ACCESSIBILITY_ALLOWED == true) {
//                                    String usscode = "*" + BALANCE_USSD_CODE + Uri.encode("#");
//                                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                                        return;
//                                    }
//                                    startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + usscode)));
//                                }else{
//
//                                    //just for toast
//                                    Toast.makeText(getApplicationContext(),"Allow for BEATME # Accessibility service",Toast.LENGTH_LONG).show();
//                                    Intent accessIntent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
//                                    startActivity(accessIntent);
//
//                                }
//                            }
//
//                    }).start();
//
//                }
//            });
//
//            return  mView;
//
//        }
//    }


    private BroadcastReceiver USSDBroadcastReceiver =new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals("balanceIntent")){
                String currentBalance=intent.getStringExtra("balanceIntent");
                if(currentBalance!=null | !currentBalance.equals("")){
                    //todo not right way change to int and check if >=10 rs.
                    if(loan_amount!=null)//never null
                        loan_amount.setText("Rs. "+currentBalance);
                }else{
                    //user level approval for accessibility services
                    Toast.makeText(getApplicationContext(),"Allow for Beatme",Toast.LENGTH_LONG).show();
                    Intent accessIntent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                    startActivity(accessIntent);
                }

            }

        }
    };



    @Override
    protected void onStart() {
        super.onStart();
        runnable=new Runnable() {
            @Override
            public void run() {
                notificationLoad();
            }

        };
    }

  @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(USSDBroadcastReceiver);
    }


    @Override
    public void onRewardedVideoAdLoaded() {
        //send the notification to ads icon
        new_ads_text.setText("New ads");
        adsHandlerTimer.removeCallbacks(adsRunnableTimer);
    }

    @Override
    public void onRewardedVideoAdOpened() {
        watching=true;

    }

    @Override
    public void onRewardedVideoStarted() {

    }

    @Override
    public void onRewardedVideoAdClosed() {
        watching=false;
        new_ads_text.setText("");
        adsHandlerTimer.postDelayed(adsRunnableTimer,10000);//after 10 seconds request next video ads


    }

    @Override
    public void onRewarded(RewardItem rewardItem) {

    }

    @Override
    public void onRewardedVideoAdLeftApplication() {

    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {

    }
}
