package com.example.swornim.beatme;

import android.app.NotificationManager;
import android.content.Context;

/**
 * Created by Swornim on 10/13/2017.
 */

 public   class  Notification {

private Context context;
    NotificationManager notificationManager;
    android.app.Notification.Builder notificationBuilder;
    android.app.Notification notification;

    public Notification(Context context) {
        this.context = context;
    }

    public void createNotification(){


         notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

//Set notification information:
       notificationBuilder = new android.app.Notification.Builder(context);
        notificationBuilder.setOngoing(true)
                .setContentTitle("Notification Content Title")
                .setContentText("Notification Content Text")
                .setProgress(100, 0, false);
//Send the notification:
        notification = notificationBuilder.build();
        notificationManager.notify(100, notification);


    }

    public void update(int value){
//Send the notification:
        notificationBuilder.setProgress(100,value,false);
    }



}
