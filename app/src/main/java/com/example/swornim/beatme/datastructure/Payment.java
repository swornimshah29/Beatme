package com.example.swornim.beatme.datastructure;

import java.util.List;

public class Payment {

    private String to;
    private String from;
    private String amount;
    private String sync;
    private String paymentStatus;
    private String paymentId;


    public class Loan{
        private List<Payment> loans;

        public List<Payment> getLoans() {
            return loans;
        }

        public void setLoans(List<Payment> loans) {
            this.loans = loans;
        }
    }


    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }


    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
}
