package com.example.swornim.beatme.Fragment;

import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.example.swornim.beatme.Details;


import java.util.ArrayList;
import java.util.List;

import com.example.swornim.musicnap.Manifest;
import com.example.swornim.musicnap.R;

/**

This is the gallery class which deals every gallery related operation
 */

public class Gallery extends Fragment {

    private FragmentManager fragmentManager;
    private ImageView giftcardid;
    private ImageView galleryid;
    private Toolbar toolbar;
    private ImageView imageholder;
    private List<Details> mList=new ArrayList<>();
    private ArrayAdapter<Details> adapter;
    private GridView mListview;
    private Display display;
    private Point point=new Point();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView= inflater.inflate(R.layout.gallery_fragement,container,false);


        adapter=new ImageAdapterLoader(getActivity(),mList);
        fragmentManager=getFragmentManager();

        display=getActivity().getWindowManager().getDefaultDisplay();
        display.getSize(point);

        mListview=(GridView) mView.findViewById(R.id.grid_view_id);
//        giftcardid=(ImageView) mView.findViewById(R.id.giftcardid);
//        galleryid=(ImageView) mView.findViewById(R.id.assignmentId);

        mListview.setAdapter(adapter);
        new AsyncImageProcess(getActivity()).execute();

//        messageid.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                MessageFragement messageFragement=new MessageFragement();
//                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
//
//                //fragment resides on activity so replace id of activity
//                fragmentTransaction.replace(R.id.activityId,messageFragement);
//                addSingleFragementInstance(fragmentManager,fragmentTransaction);
//                fragmentTransaction.commit();
//            }
//        });
//
//        giftcardid.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Gallery gallery =new Gallery();
//                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.activityId, gallery);
//
//                addSingleFragementInstance(fragmentManager,fragmentTransaction);
//                fragmentTransaction.commit();
//            }
//        });
//
//        galleryid.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Stories stories=new Stories();
//                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.activityId,stories);
//
//                addSingleFragementInstance(fragmentManager,fragmentTransaction);
//
//                fragmentTransaction.commit();
//            }
//        });


//        String fileName = "christy.png";
//        String completePath = Environment.getExternalStorageDirectory()+"/"+fileName;
//
//        File file = new File(completePath);
//        if (file.exists())
//            Log.i("mytag","file exists");
//        else
//            Log.i("mytag","file dont exists");
//
//        Uri imageUri = Uri.fromFile(file);
//
//        Glide.with(this)
//                .load(imageUri)
//                .centerCrop()
//                .into(imageholder);

        return mView;

    }

    private void addSingleFragementInstance(FragmentManager fragmentManager, FragmentTransaction fragmentTransaction){
        for(int i=0;i<fragmentManager.getBackStackEntryCount();i++){
            if(!fragmentManager.popBackStackImmediate(fragmentTransaction.getClass().getName(),0)){
                //if not in the stack then add for that particular fragments
                fragmentManager.popBackStack();
            }
        }
        fragmentTransaction.addToBackStack(fragmentTransaction.getClass().getName());
    }



    private class AsyncImageProcess extends AsyncTask<Void,Void,String> {

        private Context context;

        public AsyncImageProcess(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(Void... voids) {
            //get all the imagees url and store in the mlist container
            String[] projection = { MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.HEIGHT, MediaStore.MediaColumns.WIDTH,
                    MediaStore.Images.Media.DISPLAY_NAME,
                    MediaStore.Images.Media.SIZE,
                    MediaStore.Images.Media.DATE_ADDED};

            Cursor cursor = getActivity().getContentResolver().query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, null);

            if (cursor!=null){

                    while (cursor.moveToNext()) {

                         Details details=new Details();
                         details.setPhotoUrl(cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)));
                         details.setPhotoSize(cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.SIZE)));
                         details.setImageWidth(cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.WIDTH)));
                         details.setImageHeight(cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.HEIGHT)));
                         details.setPhotoDate(cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATE_ADDED)));
                         details.setPhotoName(cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME)));
                         mList.add(details);
                    }

            }


            if(cursor!=null)
            cursor.close();

            return "done";
        }

        @Override
        protected void onPostExecute(String s) {
            adapter.notifyDataSetChanged();


        }
    }

    private class ImageAdapterLoader extends ArrayAdapter<Details> {

        public ImageAdapterLoader(Context context, List<Details> mList) {
            super(context, R.layout.gallery_template,mList);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View mView=convertView;
            if (convertView==null){
                LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                mView = layoutInflater.inflate(R.layout.gallery_template, parent, false);
            }

            ImageView gallery_template_imageid=(ImageView) mView.findViewById(R.id.gallery_template_imageid);
//            TextView gallery_template_textid=(TextView) mView.findViewById(R.id.gallery_template_textid);


            gallery_template_imageid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                }
            });


            gallery_template_imageid.getLayoutParams().height=(point.y)/3;
            gallery_template_imageid.getLayoutParams().width=(point.x)/3;
//            gallery_template_textid.setText(mList.get(position).getPhotoName());

            Glide.with(getActivity()).load(mList.get(position).getPhotoUrl()).centerCrop().into(gallery_template_imageid);

            return mView;
        }
    }


}
