package com.example.swornim.beatme;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.swornim.musicnap.R;

import java.util.List;

/**
 * Created by Swornim on 4/29/2017.
 */

public class UserGiftsAdapter extends ArrayAdapter<MinorDetails> {

    private List<MinorDetails> postObjects;

    public UserGiftsAdapter(Context context, List<MinorDetails> postObjects){

        super(context, R.layout.usergiftstemplate,postObjects);
        this.postObjects=postObjects;

    }


        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {
            View mView=convertView;
            if(convertView==null) {
                LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                mView = layoutInflater.inflate(R.layout.usergiftstemplate, parent, false);
            }

            TextView usergiftsCardNumber=(TextView) mView.findViewById(R.id.usergiftsCardNumber);
            TextView usergiftsCardValidation=(TextView) mView.findViewById(R.id.usergiftsCardValidation);
            TextView usergiftsCardType=(TextView) mView.findViewById(R.id.usergiftsCardType);



                usergiftsCardNumber.setText(postObjects.get(position).getRechargeCardNumber());
                usergiftsCardValidation.setText(postObjects.get(position).getRecharCardValidation());
               usergiftsCardType.setText(postObjects.get(position).getRecgargeCardType());

            return mView;
        }




}
