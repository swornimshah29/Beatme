package com.example.swornim.beatme.Fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.swornim.beatme.MSharedPreferences;
import com.example.swornim.beatme.MinorDetails;
import com.example.swornim.beatme.Newsfeed;
import com.example.swornim.beatme.ServerSideTask;
import com.example.swornim.musicnap.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/*This class deals with the news feed updates*/

public class SideBarHomeFragment extends Fragment {

    private ArrayAdapter<MinorDetails> adapter;
    private ListView mListView;
    public List<MinorDetails> postObjects=new ArrayList<>();




    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadCastReceiver,new IntentFilter("myEventName"));
        View mView= inflater.inflate(R.layout.fragment_side_bar_home,container,false);
        mListView=(ListView) mView.findViewById(R.id.mListView);
        adapter=new Newsfeed(getContext(),postObjects);
        mListView.setAdapter(adapter);

        MinorDetails minorDetails=new MinorDetails();
        minorDetails.setFunctionCalledForOperation("fetchTheCurrentJsonObject");
        new ServerSideTask(getContext(),minorDetails).execute();


        return mView;



    }


    private BroadcastReceiver mBroadCastReceiver =new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            final String fetchJsonData=new MSharedPreferences(getContext()).getSharedPref("fetchJsonData");
            //process the json data and update the adapter for new  newsfeedtemplate

            try {
                JSONObject root = new JSONObject(fetchJsonData);
                JSONArray events=root.getJSONArray("events");
                postObjects.clear();
                for(int i=0;i<events.length();i++){
                    MinorDetails tempObject=new MinorDetails();
                    JSONObject eachObject=events.getJSONObject(i);
                    tempObject.setLeftTeamName(eachObject.getString("leftTeamName"));
                    tempObject.setRightTeamName(eachObject.getString("rightTeamName"));
                    tempObject.setEventName(eachObject.getString("eventName"));
                    tempObject.setEventId(eachObject.getString("eventId"));
                    tempObject.setDate(eachObject.getString("date"));
                    postObjects.add(tempObject);
                }
                adapter.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mBroadCastReceiver);
    }


}

