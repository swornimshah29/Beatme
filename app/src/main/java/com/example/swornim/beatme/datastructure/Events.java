package com.example.swornim.beatme.datastructure;

import java.io.Serializable;
import java.util.List;

public class Events implements Serializable{

    private List<Epl> epl;

    public List<Epl> getEpl() {
        return epl;
    }

    public void setEpl(List<Epl> epl) {
        this.epl = epl;
    }

    public class Epl{
        private String userNumber;
        private String userName;
        private String tick;
        private String opponentNumber;
        private String opponentName;
        private String leftTeamName;
        private String rightTeamName;
        private String amount;
        private String eventId;



        public String getTick() {
            return tick;
        }

        public void setTick(String tick) {
            this.tick = tick;
        }

        public String getLeftTeamName() {
            return leftTeamName;
        }

        public void setLeftTeamName(String leftTeamName) {
            this.leftTeamName = leftTeamName;
        }

        public String getUserNumber() {
            return userNumber;
        }

        public void setUserNumber(String userNumber) {
            this.userNumber = userNumber;
        }

        public String getOpponentNumber() {
            return opponentNumber;
        }

        public void setOpponentNumber(String opponentNumber) {
            this.opponentNumber = opponentNumber;
        }

        public String getRightTeamName() {
            return rightTeamName;
        }

        public void setRightTeamName(String rightTeamName) {
            this.rightTeamName = rightTeamName;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getEventId() {
            return eventId;
        }

        public void setEventId(String eventId) {
            this.eventId = eventId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getOpponentName() {
            return opponentName;
        }

        public void setOpponentName(String opponentName) {
            this.opponentName = opponentName;
        }
    }



    //add other classes accordingly

}
