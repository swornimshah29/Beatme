package com.example.swornim.beatme.Fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.sax.StartElementListener;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.swornim.beatme.GlobalNewsfeed;
import com.example.swornim.beatme.MSharedPreferences;
import com.example.swornim.beatme.MinorDetails;
import com.example.swornim.beatme.ServerSideTask;
import com.example.swornim.musicnap.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Swornim on 6/2/2017.
 */

public class SideBarPointsFragment extends Fragment {
    private TextView mTextViewPoints;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadCastReceiverPoints,new IntentFilter("userPoints"));
        View mView = inflater.inflate(R.layout.fragementsidebarpoints, container, false);
        mTextViewPoints=(TextView) mView.findViewById(R.id.userPointsId);
        MinorDetails minorDetails = new MinorDetails();
        minorDetails.setFunctionCalledForOperation("loadUserPoints");
        new ServerSideTask(getContext(), minorDetails).execute();

        return mView;

    }


    private BroadcastReceiver mBroadCastReceiverPoints =new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String userPoints=new MSharedPreferences(getActivity()).getSharedPref("pointsDetails");
            mTextViewPoints.setText(userPoints);

        }
    };

    @Override
    public void onDestroyView() {
        //may need to unregisteredreceiver or else error might occur
        super.onDestroyView();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mBroadCastReceiverPoints);

    }


}
