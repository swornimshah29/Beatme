package com.example.swornim.beatme;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.swornim.musicnap.R;

public class RegistrationActivity extends AppCompatActivity {

    private TextView dualSimTextView,singleSimTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dualsim);

        dualSimTextView=(TextView) findViewById(R.id.dualSimTextview);
        singleSimTextView=(TextView) findViewById(R.id.singleSimTextview);

        dualSimTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Dialog dialog = new Dialog(RegistrationActivity.this, android.R.style.Theme_Wallpaper_NoTitleBar_Fullscreen);
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        //         LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(LoanPaymentBroadcastreceiver);

                    }
                });
                dialog.setContentView(R.layout.dual_sim_dialog_number);
                dialog.setCancelable(false);
                dialog.show();

                final EditText number1=dialog.findViewById(R.id.dualNcellNumber1);
                final EditText number2=dialog.findViewById(R.id.dualNcellNumber2);
                Button registerDualSimNumber=dialog.findViewById(R.id.registerDualSimNumber);
                final ProgressBar dualSimProgressBar=dialog.findViewById(R.id.dualSimdialogProgressBar);

                registerDualSimNumber.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(number1!=null && number2!=null && !number1.equals("") && !number2.equals("")){
                            if(number1.getText().length()==10 && number2.getText().length()==10){
                                //send the data to the server but first check if he or she is online or not
                                dualSimProgressBar.setVisibility(View.VISIBLE);
                            }

                        }
                    }
                });



            }
        });

        singleSimTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Dialog dialog = new Dialog(RegistrationActivity.this, android.R.style.Theme_Wallpaper_NoTitleBar_Fullscreen);
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        //         LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(LoanPaymentBroadcastreceiver);

                    }
                });
                dialog.setContentView(R.layout.single_sim_dialog_number);
                dialog.setCancelable(false);
                dialog.show();

                final EditText number=dialog.findViewById(R.id.singleNcellNumber);
                Button registerSingleNumber=dialog.findViewById(R.id.registerSingleNumber);
                final ProgressBar singleSimdialogProgressBar=dialog.findViewById(R.id.singleSimdialogProgressBar);
                registerSingleNumber.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(number!=null  && !number.equals("")){
                            if(number.getText().length()==10){
                                //send the data to the server but first check if he or she is online or not
                                singleSimdialogProgressBar.setVisibility(View.VISIBLE);
                                startActivity(new Intent(RegistrationActivity.this,RegisterActivity.class));
                            }

                        }
                    }
                });

            }
        });






    }
}
