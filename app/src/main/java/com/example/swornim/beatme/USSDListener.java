package com.example.swornim.beatme;
import org.apache.commons.lang3.StringUtils;
import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Intent;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import java.util.Collections;
import java.util.List;

/**
 * Created by Swornim on 9/9/2017.
 */


    public class USSDListener extends AccessibilityService {

    private static String PHONENUMBER;
    public static String whichUSSD=null;
    private static String BALANCE=null;
    private boolean USSD_FINISHED=false;

        public String TAG = USSDListener.class.getSimpleName();

        @Override
        public void onAccessibilityEvent(AccessibilityEvent event) {

//            Toast.makeText(getApplicationContext(),"Ussd is called",Toast.LENGTH_LONG).show();
            AccessibilityNodeInfo source = event.getSource();
    /* if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED && !event.getClassName().equals("android.app.AlertDialog")) { // android.app.AlertDialog is the standard but not for all phones  */

            if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED && !String.valueOf(event.getClassName()).contains("AlertDialog")) {
                return;
            }
            if(event.getEventType() == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED && (source == null || !source.getClassName().equals("android.widget.TextView"))) {
                return;
            }
            if(event.getEventType() == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED && TextUtils.isEmpty(source.getText())) {
                return;
            }

            List<CharSequence> eventText;

            if(event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) {
                eventText = event.getText();
            } else {
                eventText = Collections.singletonList(source.getText());
            }

            String text = processUSSDText(eventText);

            if(TextUtils.isEmpty(text)) return;
            // Close dialog explicitly
            // Handle USSD response here
            performGlobalAction(GLOBAL_ACTION_BACK); // This works on 4.1+ only
            Log.i("mytag", "accessibiity service called" );


            if(whichUSSD!=null){
                switch (whichUSSD){
                    case "phoneNumberIntent":
                        findPhoneNumber(text);
                        break;

                    case "balanceIntent":
                        findBalance(text);
                        sendBalance("");
//                        receiveBalance();
                        break;

                    case "sendBalanceIntent":
                        sendBalance(text);
                        break;

                        default:
                            break;
                }
            }

        }

        private String processUSSDText(List<CharSequence> eventText) {
            for (CharSequence s : eventText) {
                String text = String.valueOf(s);
                Log.i("mytag", "response data "+String.valueOf(eventText) );

                // Return text if text is the expected ussd response
                if( true ) {
                    return text;
                }
            }
            return null;
        }

        @Override
        public void onInterrupt() {
            Log.i("mytag", "oninterupt called " );

        }

        @Override
        protected void onServiceConnected() {
            super.onServiceConnected();
            AccessibilityServiceInfo info = new AccessibilityServiceInfo();
            info.flags = AccessibilityServiceInfo.DEFAULT;
            info.packageNames = new String[]{"com.android.phone"};
            info.eventTypes = AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED | AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED;
            info.feedbackType = AccessibilityServiceInfo.FEEDBACK_GENERIC;
            setServiceInfo(info);
            Log.i("mytag", "onservice connected" );

        }


    @Override
    public boolean onUnbind(Intent intent) {
        Log.i("mytag", "onunbind called " );

        return super.onUnbind(intent);

    }

    public void findPhoneNumber(String ussdText){

            if(ussdText!=null) {

//                Log.i("mytag", "first filter " + ussdText);
                ussdText = ussdText.replaceAll("[-=+()/!~|*&^#@$:'{};%`.,]", "");
//                Log.i("mytag", "second filter " + ussdText);

                ussdText = ussdText.replaceAll("[^0-9]+", "");

                if(ussdText!=null){
                    if(ussdText.length()>=10 && ussdText.charAt(0)=='9' && ussdText.charAt(1)=='8'){
                        PHONENUMBER = ussdText.substring(0, Math.min(ussdText.length(), 10));
                        PHONENUMBER = PHONENUMBER.replaceAll("\\s+", "");
                        new MSharedPreferences(getApplicationContext()).setSharedPref("USER_PHONE_NUMBER",PHONENUMBER);
                        Intent intent=new Intent("phoneNumberIntent");
                        intent.putExtra("phoneNumberIntent",PHONENUMBER);
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                    }

                }
            }
        }


        public  void findBalance(String ussdText){
            //not good for if both the ncell sim card

            if(ussdText!=null) {
                ussdText = ussdText.toLowerCase();
//                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                if (!ussdText.equals("") && ussdText.contains("balance")) {
                    BALANCE = StringUtils.substringBetween(ussdText, "rs.", ".");
                    BALANCE = BALANCE.replaceAll("\\s+", "");
                        Log.i("mytag", " BALANCE " + BALANCE);
                        Intent intent=new Intent("balanceIntent");
                        intent.putExtra("balanceIntent",BALANCE);
//                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                }
            }else{
                Log.i("mytag", " NO RESPONSE CAME FROM THE USSD ");

            }

        }


        private void sendBalance(String ussdText) {

            String ussd = "Dear customer,You have transferred Rs.8.75 to 9813847444.Rs. 1.25 has been deducted as service fee.";
            String checkFormat = ussd;//todo replace with ussdText
            checkFormat = checkFormat.toLowerCase();
            checkFormat = checkFormat.replaceAll("\\s+", "");
            if (checkFormat.contains("youhavetransferred") || checkFormat.contains("deductedasservicefee") || checkFormat.contains("deducted")) {
                String amount = StringUtils.substringBetween(checkFormat, "rs.", "to");
                String trialNumber1[] = StringUtils.substringsBetween(checkFormat, "to", ".");

                for(String each:trialNumber1){
                    //todo also add whether 977-ncell phonenumber format is present or not
                    if(each.contains("981") && each.length()==10){
                        Log.i("mytag", "phonenumber is " + each);
                        Intent intent=new Intent("sendBalanceIntent");
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                        break;
                    }
                }
                Log.i("mytag", "amount of " + amount);

            }

        }


        private void receiveBalance(){
            String message="Dear Customer,9818142227 has transferred Rs. 8.75 to you.";
            //todo replace with ussdtext
            message=message.toLowerCase();
            message=message.replaceAll("\\s+","");
            String words[]=StringUtils.substringsBetween(message,"dear","transferred");
            Log.i("mytag", "words "+words);
            Log.i("mytag", "message is  "+message);

            for(String each: words){
                if(each!=null){
                    if(each.contains("981") || each.contains("98")){
                        each = each.replaceAll("[^0-9]+", "");
                        if(each.contains("981") && each.length()==10) {
                            Log.i("mytag", "sender number is " + each);
                            break;
                        }

                    }

                }

            }


        }


        //register the dynamic broadcast within the service




}




