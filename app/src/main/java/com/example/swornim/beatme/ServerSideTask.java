package com.example.swornim.beatme;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.content.ContentResolverCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.swornim.beatme.datastructure.DatabaseDetails;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.internal.tls.TrustRootIndex;

/**
 * Created by Swornim on 4/9/2017.
 * This class deals with all kinds of server side networking stuffs
 */

public class ServerSideTask extends AsyncTask<Void,Void,String> {

    private Context mContext;
    private String mResult=null;
    private MinorDetails minorDetails;
    private Runnable runnable;
    private Handler handler=new Handler();
    private List<DatabaseDetails> adapterSource=new ArrayList<>();


    public ServerSideTask(Context mcontext, MinorDetails minorDetails) {
        this.minorDetails=minorDetails;
        this.mContext=mcontext;
    }


    @Override
    protected String doInBackground(Void... voids) {
        //just apply the switch or the if else statement

        switch (minorDetails.getFunctionCalledForOperation()){
            case "fetchTheCurrentJsonObject":
                fetchTheCurrentJsonObject();
                break;
            case "saveUserBeatEvents":
                saveUserBeatEvents();
                break;
            case "UserNotification":
                UserNotification();
                break;

            case "mNotificationEvents":
                mNotificationEvents();
                break;

            case "registrationProcess":
                registrationProcess();
                break;

            case "globalNewsfeedFetch":
                globalNewsfeedFetch();
                break;

            case "LoadGifts":
                LoadGifts();
                break;

            case "loadUserPoints":
                loadUserPoints();
                break;

            case "Loans":
                Loans();
                break;

            case "clearLoan":
                clearLoan();
                break;

            case "newsfeed":
                newsfeed();
                break;

            case "events":
                events();
                break;

            case "submitProduct":
                submitProduct();
                break;

            case "loadData":
                loadData();
                break;

            case "notificationLoad":
                notificationLoad();
                break;

            default:
                //do nothing

        }
        return mResult;
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onPostExecute(String mResult) {
        //process the json data and update the newsfeedtemplate again
        //Todo Dont use this object sending like adapter instead use broadcaster or other technique


    }

    private void fetchTheCurrentJsonObject(){

        RequestQueue requestQueue= Volley.newRequestQueue(mContext);

        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                "http://192.168.1.104/beatme/EventsSource.php",
//                "http://digitate-convention.000webhostapp.com/beatme/EventsSource.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        mResult=response.toString();
                        Log.i("mytag",mResult);
                        new MSharedPreferences(mContext).setSharedPref("fetchJsonData",mResult);
                        Intent intent=new Intent("myEventName");
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("mytag", "something went wrong during the fetch process"+error);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                map.put("fetchJsonData","fetch");

                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> header=new HashMap<>();
                header.put("Content-Type","application/json");
                return header;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void saveUserBeatEvents(){

        RequestQueue requestQueue= Volley.newRequestQueue(mContext);
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
//                "http://digitate-convention.000webhostapp.com/beatme/SavedEvents.php",
                "http://192.168.1.104/beatme/BeatQueue.php",

                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mResult=response.toString();
                        Log.i("mytag",mResult);

                        Toast.makeText(mContext,mResult,Toast.LENGTH_LONG).show();
                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("mytag", "Something went wrong while saving events " +error);
                saveUserBeatEvents();//resend when error

            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                //send the json format

                Gson gson=new Gson();
                return map;


            }

        };
        requestQueue.add(stringRequest);
    }

    private void UserNotification(){

        RequestQueue requestQueue= Volley.newRequestQueue(mContext);
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
//                "http://digitate-convention.000webhostapp.com/beatme/UserNotification.php",
                "http://192.168.1.104/beatme/UserNotification.php",

                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        mResult=response.toString();
                        new MSharedPreferences(mContext).setSharedPref("MyStatusReportEvent",mResult);
                        Intent intent=new Intent("MyStatusReportEvent");
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("mytag", "something went wrong during the Opponent finding process");

            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                map.put("userName","swornim bikram shah");
                map.put("uniqueUserId","12412124");

                return map;
            }

        };
        requestQueue.add(stringRequest);
    }

    private void mNotificationEvents(){

        RequestQueue requestQueue= Volley.newRequestQueue(mContext);
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                "http://digitate-convention.000webhostapp.com/beatme/StatusReport.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        mResult=response.toString();
                        Log.i("mytag","This is the notification"+ mResult);
                        Log.i("mytag",mResult);

                        new MSharedPreferences(mContext).setSharedPref("myStatusReport",mResult);
                        Intent intent=new Intent("myEventName");
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);



                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("mytag", "something went wrong during the Opponent finding process");

            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                map.put("userName","Swornim bikram shah");
                map.put("uniqueUserId","12412124");

                return map;
            }

        };
        requestQueue.add(stringRequest);

    }

    private void registrationProcess(){

        RequestQueue requestQueue= Volley.newRequestQueue(mContext);
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
//                "http://digitate-convention.000webhostapp.com/beatme/UserRegisteration.php",
                "http://192.168.1.104/beatme/UserRegistration.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        mResult=response.toString();
                        Log.i("mytag",mResult);

                        new MSharedPreferences(mContext).setSharedPref("registrationDetail",mResult);
                        Intent intent=new Intent("myRegistration");
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);



                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("mytag", "resending the data");
                registrationProcess();

            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                map.put("userName",minorDetails.getUserName());
                map.put("phoneNumber","9813847444");
                map.put("uniqueUserId",minorDetails.getUserUniqueId());

                return map;
            }

        };
        requestQueue.add(stringRequest);

    }

    private void globalNewsfeedFetch(){

        RequestQueue requestQueue= Volley.newRequestQueue(mContext);
        StringRequest stringRequest=new StringRequest(Request.Method.GET,
                "http://digitate-convention.000webhostapp.com/beatme/GlobalNewsfeed.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        mResult=response.toString();
                        Log.i("mytag",mResult);
                        new MSharedPreferences(mContext).setSharedPref("globalNewsfeed",mResult);
                        Intent intent=new Intent("globalNewsfeed");
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("mytag", "something went wrong during the fetch process"+error);
            }
        }){

        };
        requestQueue.add(stringRequest);
    }

    private void LoadGifts(){

        RequestQueue requestQueue= Volley.newRequestQueue(mContext);
        StringRequest stringRequest=new StringRequest(Request.Method.GET,
                "http://digitate-convention.000webhostapp.com/beatme/Gifts.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        mResult=response.toString();
                        Log.i("mytag",mResult);
                        new MSharedPreferences(mContext).setSharedPref("giftsDetails",mResult);
                        Intent intent=new Intent("giftsDetails");
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("mytag", "something went wrong gifts process"+error);
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                map.put("userName",new MSharedPreferences(mContext).getSharedPref("userName"));
                map.put("uniqueUserId",new MSharedPreferences(mContext).getSharedPref("uniqueUserId"));

                return map;
            }

        };
        requestQueue.add(stringRequest);
    }
private void loadUserPoints(){

        RequestQueue requestQueue= Volley.newRequestQueue(mContext);
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                "http://olc.worldbank.org",
//                "http://digitate-convention.000webhostapp.com/beatme/UserPoints.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        mResult=response.toString();
                        Log.i("mytag",mResult);
                        new MSharedPreferences(mContext).setSharedPref("pointsDetails",mResult);
                        Intent intent=new Intent("userPoints");
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("mytag", "something went wrong User Points"+error);
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                map.put("userName",new MSharedPreferences(mContext).getSharedPref("userName"));
                map.put("uniqueUserId",new MSharedPreferences(mContext).getSharedPref("uniqueUserId"));
                map.put("phoneNumber",new MSharedPreferences(mContext).getSharedPref("userPhoneNumber"));

                return map;
            }

        };
        requestQueue.add(stringRequest);
    }



      private void loadCompetitorsData(){

        RequestQueue requestQueue= Volley.newRequestQueue(mContext);
        StringRequest stringRequest=new StringRequest(Request.Method.GET,
                "http://digitate-convention.000webhostapp.com/beatme/Competion.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        mResult=response.toString();
                        Log.i("mytag",mResult);
                        new MSharedPreferences(mContext).setSharedPref("CompetitionDetails",mResult);
                        Intent intent=new Intent("CompetitionIntent");
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("mytag", "something went wrong User Points"+error);
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                //no thing is sent just a request for the data
//                map.put("userName",new MSharedPreferences(mContext).getSharedPref("userName"));
//                map.put("uniqueUserId",new MSharedPreferences(mContext).getSharedPref("uniqueUserId"));
//                map.put("phoneNumber",new MSharedPreferences(mContext).getSharedPref("userPhoneNumber"));

                return map;
            }

        };
        requestQueue.add(stringRequest);
    }


    //this gets the social newsfeed data
      private void newsfeed(){

        RequestQueue requestQueue= Volley.newRequestQueue(mContext);
        StringRequest stringRequest=new StringRequest(Request.Method.GET,
//                "http://digitate-convention.000webhostapp.com/beatme/Competion.php",
                "http://192.168.1.104/beatme/Newsfeed.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        mResult=response.toString();
                        Log.i("mytag",mResult);
                        new MSharedPreferences(mContext).setSharedPref("NewsfeedSharedpref",mResult);
                        Intent intent=new Intent("NewsfeedIntent");
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("mytag", "something went wrong in newsfeed"+error);
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();


                return map;
            }

        };
        requestQueue.add(stringRequest);
    }

    private void Loans(){

//        RequestQueue requestQueue= Volley.newRequestQueue(mContext);
//        StringRequest stringRequest=new StringRequest(Request.Method.POST,
////                "http://digitate-convention.000webhostapp.com/beatme/Competion.php",
//                "http://192.168.1.104/beatme/UserNotification.php",
//                new com.android.volley.Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//
//                        mResult=response.toString();
//                        Log.i("mytag",mResult);
//
////                        new MSharedPreferences(mContext).setSharedPref("loansJson",mResult);
//                        Intent intent=new Intent("loanPaymentIntent");
//                        intent.putExtra("loanPaymentIntent", mResult);
//                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
//
//                    }
//                }, new com.android.volley.Response.ErrorListener() {
//
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.i("mytag", "something went wrong User Points"+error);
//            }
//        }){
//
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//
//                Map<String,String> map=new HashMap<>();
//                String uniqueId=new MSharedPreferences(mContext).getSharedPref("uniqueUserId");
//                map.put("uniqueId",uniqueId);
//                map.put("loan","fetch");
//
//                return map;
//            }
//
//        };
//        requestQueue.add(stringRequest);
        //todo fetch all the loans from firebase and store in the offline database for future uses
        FirebaseDatabase.getInstance().getReference().child("users/phonenumber1/bets")
                .orderByChild("paymentstatus")
                .equalTo("unclear")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.i("tags",dataSnapshot.getValue()+"");

                        for(DataSnapshot each:dataSnapshot.getChildren()){
                            Log.i("tags",each.getKey());
                            Log.i("tags",each.getValue()+"");
                            DatabaseDetails eachBets=each.getValue(DatabaseDetails.class);
                            Log.i("tags","event id is "+eachBets.getEventId());
                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }

private void clearLoan(){

        RequestQueue requestQueue= Volley.newRequestQueue(mContext);
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
//                "http://digitate-convention.000webhostapp.com/beatme/Competion.php",
                "http://192.168.1.104/beatme/loan_result.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        mResult=response.toString();
                        Log.i("mytag","cleared loans"+mResult);

//                        new MSharedPreferences(mContext).setSharedPref("loans",mResult);
//                        Intent intent=new Intent("loans");
//                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("mytag", "something went wrong User Points"+error);
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                map.put("uniqueId",minorDetails.getUserUniqueId());
                map.put("userName",minorDetails.getOpponentUserName());
                map.put("phoneNumber",minorDetails.getOpponentNumber());
                map.put("eventId",minorDetails.getEventId());

                return map;
            }

        };
        requestQueue.add(stringRequest);
    }

private void events(){

        RequestQueue requestQueue= Volley.newRequestQueue(mContext);
        StringRequest stringRequest=new StringRequest(Request.Method.GET,
//                "http://digitate-convention.000webhostapp.com/beatme/Competion.php",
                "http://192.168.1.111/betme/Events.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        mResult=response;
                        Log.i("mytag","epl json"+mResult);
//
                        new MSharedPreferences(mContext).setSharedPref("eventJsonString",mResult);
                        Intent intent=new Intent("intentEvent");
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("mytag", "something went wrong User Points"+error);
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                map.put("hello","adsad");

                return map;
            }


        };
        requestQueue.add(stringRequest);
    }


    private void submitProduct(){

        RequestQueue requestQueue= Volley.newRequestQueue(mContext);
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
//                "http://digitate-convention.000webhostapp.com/beatme/Competion.php",
                "http://192.168.1.104/bussinessLogic/user_product_events.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        mResult=response;
                        Log.i("mytag","epl json"+mResult);
//
                        new MSharedPreferences(mContext).setSharedPref("submitProductResponse",mResult);
                        Intent intent=new Intent("productIntent");
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("mytag", "something went wrong User Points"+error);
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                map.put("productUrl",minorDetails.getProductUrl());

                return map;
            }


        };
        requestQueue.add(stringRequest);
    }


    private void loadData(){

                RequestQueue requestQueue = Volley.newRequestQueue(mContext);
                StringRequest stringRequest = new StringRequest(Request.Method.POST,
//                "http://digitate-convention.000webhostapp.com/beatme/Competion.php",
                        "http://192.168.1.104/bussinessLogic/new_events_for_clients.php",
                        new com.android.volley.Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                String mResult = response;
//
                                Intent intent = new Intent("MyEvents");
                                intent.putExtra("MyEventsJsonString",mResult);

                                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                                handler.removeCallbacks(runnable);

                            }
                        }, new com.android.volley.Response.ErrorListener() {


                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("mytag", "Resending the data");
                        runnable=new Runnable() {
                            @Override
                            public void run() {
                                loadData();
                            }

                        };

                        handler.postDelayed(runnable,40000);
                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> map = new HashMap<>();
                        map.put("uniqueId", new MSharedPreferences(mContext).getSharedPref("uniqueId"));
                        map.put("pendingEvents","yup");

                        return map;
                    }


                };
                requestQueue.add(stringRequest);


    }

    private void notificationLoad(){

                RequestQueue requestQueue = Volley.newRequestQueue(mContext);
                StringRequest stringRequest = new StringRequest(Request.Method.POST,
//                "http://digitate-convention.000webhostapp.com/beatme/Competion.php",
                        "http://192.168.1.104/bussinessLogic/new_events_for_clients.php",
                        new com.android.volley.Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                String mResult = response;
//
                                Intent intent = new Intent("notificationIntent");
                                intent.putExtra("MyNotificationJson",mResult);
                                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                                if(runnable!=null){
                                    handler.removeCallbacks(runnable);
                                }

                            }
                        }, new com.android.volley.Response.ErrorListener() {


                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("mytag", "Resending the data");
                        runnable=new Runnable() {
                            @Override
                            public void run() {
                                notificationLoad();
                            }

                        };

                        handler.postDelayed(runnable,100);
                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> map = new HashMap<>();
                        map.put("uniqueId", new MSharedPreferences(mContext).getSharedPref("uniqueId"));
                        map.put("pendingEvents","yup");

                        return map;
                    }


                };
                requestQueue.add(stringRequest);

    }

    private void updateContacts() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Cursor cursor=null;

                String[] projections=
                        {
                                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                                ContactsContract.CommonDataKinds.Phone.NUMBER,
                                ContactsContract.CommonDataKinds.Phone._ID,


                        };

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    cursor = mContext.
                            getContentResolver().query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,null,null);
                }else{
                    cursor= ContentResolverCompat.
                            query(mContext.getContentResolver(),ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,null,null,null,null);
                }

                if(cursor!=null){
                    while(cursor.moveToNext()) {

                        DatabaseDetails each=new DatabaseDetails();
                        each.setFriendName(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
//                        each.setPhoneImageUrl(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI)));
                        String filteredNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        if (filteredNumber != null) {
                            filteredNumber = filteredNumber.replaceAll("[-=+()/!~|*&^#@$:'{}_abcdefghijklmnopqrstuvwxyz;%`.,]", "");

                            if (filteredNumber.length() > 10) {
                                //remove the first n characters
                                filteredNumber = filteredNumber.substring(filteredNumber.length() - 10);
                                each.setFriendNumber(filteredNumber);
                                adapterSource.add(each);
                            }
                        }
                    }

                    /*filter the duplicates objects*/
                    for(int i=0;i<adapterSource.size();i++){
                        for(int j=0;j<adapterSource.size();j++){
                            if(i!=j) {//dont check urself
                                if (adapterSource.get(i).getFriendNumber().equals(adapterSource.get(j).getFriendNumber())) {
                                    adapterSource.remove(j);
                                }
                            }
                        }
                        Log.i("tags","phonenumber "+adapterSource.get(i).getFriendNumber());
                    }

                    Map<String,List<DatabaseDetails>> friends=new HashMap<>();
                    friends.put("freinds",adapterSource);
                    FirebaseDatabase.getInstance().getReference().child("friends").setValue(friends).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Log.i("tags","synced completed just now");
                        }
                    });

                }

            }
        }).start();

    }







}
