package com.example.swornim.beatme;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Swornim on 9/10/2017.
 */

public class FirebaseDetails {

    //firebase database
    private String userName;
    private String phoneNumber;
    private String balance;
    private String password;

    private List<MinorDetails> loans=new ArrayList<>();

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<MinorDetails> getLoans() {
        return loans;
    }

    public void setLoans(List<MinorDetails> loans) {
        this.loans = loans;
    }

}
