package com.example.swornim.beatme;

import android.app.*;
import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.example.swornim.musicnap.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Swornim on 10/13/2017.
 */

public class MService extends Service {
    private int randomNumber;
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mNotificationBuilder;
    private Notification notification;
    private PendingIntent pendingIntent;
    private PendingIntent cancelIntent;
    private boolean ondestroyedCalled=false;
    private StorageReference storageReference;



    @Override
    public void onCreate() {
        //intialize local broadcastreceiver for first time
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(ProductResponseReceiver,new IntentFilter("productIntent"));

        mNotificationManager=(NotificationManager) getApplicationContext().getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
        mNotificationBuilder=new NotificationCompat.Builder(getApplicationContext());
        pendingIntent=PendingIntent.getActivity(getApplicationContext(),0,new Intent(MService.this,tabactivity.class),0);
        cancelIntent=PendingIntent.getActivity(getApplicationContext(),0,new Intent(MService.this,RegisterActivity.class),0);

        mNotificationBuilder.setSmallIcon(R.mipmap.ic_notifications_black_48dp)
                .setWhen(5)
                .setContentTitle("Uploading Photo Please Wait")
                .setContentText("Please wait")
                .setContentIntent(pendingIntent);
        notification= mNotificationBuilder.build();
    }

    @Override
    public void onDestroy() {
        ondestroyedCalled=true;
        Log.i("mytag","Service is destroyed");

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
//        startForeground(100,notification);
////
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                //firebase uploader listener
//                Log.i("mytag","data "+ intent.getStringExtra("url"));
//
//                storageReference= FirebaseStorage.getInstance().getReference().child("sponsers_products/"+intent.getStringExtra("imageName"));
//                UploadTask uploadTask=storageReference.putFile(Uri.parse(intent.getStringExtra("url")));
//                uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
//                    @Override
//                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
//                        Log.i("mytag","data "+ task.getResult().getDownloadUrl());
//                        mNotificationBuilder.setContentText("Upload Completed");
//                        mNotificationManager.notify(100,mNotificationBuilder.build());
//
//                        MinorDetails minorDetails=new MinorDetails();
//                        minorDetails.setProductUrl(task.getResult().getDownloadUrl().toString());
//                        minorDetails.setFunctionCalledForOperation("submitProduct");
//                        new ServerSideTask(getApplicationContext(),minorDetails).execute();
//
//                    }
//                });
//
//                uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
//                    @Override
//                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
//
//                        double progress=(100.0*taskSnapshot.getBytesTransferred())/(taskSnapshot.getTotalByteCount());
//                        progress=(int)progress;
//                        Log.i("mytag","data "+ progress);
//                        Log.i("mytag","data "+ taskSnapshot.getTotalByteCount());
//
//                        mNotificationBuilder.setProgress(100,(int)progress,false);//to change the line
//                        mNotificationBuilder.setContentText((int)progress+"%");
//                        mNotificationManager.notify(100,mNotificationBuilder.build());
//
//
//
//                    }
//                });
//
//            }
//
//        }).start();
//        Log.i("mytag","SERVICE thread id"+Thread.currentThread().getId());

        return START_NOT_STICKY;
    }

    public int getRandomNumber(){
        return randomNumber;
    }


    private BroadcastReceiver ProductResponseReceiver =new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("mytag","product intent respone"+intent.getAction());
            Log.i("mytag", new MSharedPreferences(context).getSharedPref("submitProductResponse"));

        }
    };


}
