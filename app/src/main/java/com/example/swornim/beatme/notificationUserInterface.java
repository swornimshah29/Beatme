package com.example.swornim.beatme;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.swornim.musicnap.R;

import java.util.List;

/**
 * Created by Swornim on 4/24/2017.
 */

public class notificationUserInterface extends ArrayAdapter<MinorDetails> {
    private Context context;
    private List<MinorDetails> postObjects;

    public notificationUserInterface(Context context, List<MinorDetails> postObjects) {
        super(context, R.layout.fragment_side_bar_notification, postObjects);
        this.context = context;
        this.postObjects = postObjects;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View mView=convertView;
        if(convertView==null){
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            mView = layoutInflater.inflate(R.layout.friends_notification_template, parent, false);
        }

        TextView looserName=(TextView) mView.findViewById(R.id.looserName);
        TextView winnerName=(TextView) mView.findViewById(R.id.winnerName);
        TextView statusOfBeat=(TextView) mView.findViewById(R.id.statusOfBeat);

        ImageView leftImage=(ImageView) mView.findViewById(R.id.sideBarNotificationLeftImage);
        ImageView rightImage=(ImageView) mView.findViewById(R.id.sideBarNotificationRightImage);

        looserName.setText("@"+postObjects.get(position).getLooserName());
        winnerName.setText("@"+postObjects.get(position).getWinnerName());


        statusOfBeat.setText(postObjects.get(position).getStatusOfBeat());

        leftImage.setImageResource(R.drawable.arsenal);
        rightImage.setImageResource(R.drawable.liverpool);


        return mView;
    }


}

