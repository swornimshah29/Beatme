package com.example.swornim.beatme;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsMessage;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.swornim.beatme.Interface.SmsListener;
import com.example.swornim.beatme.datastructure.Payment;
import com.example.swornim.beatme.datastructure.Status;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class SmsReceiver extends BroadcastReceiver {
    private static  SmsListener mListener;
    public static Context context;
    private int REPEAT=-1;
    @Override
    public void onReceive(Context context, Intent intent) {


        Intent balance=new Intent();
        balance.setAction("paymentOnlineSuccessIntent");
        balance.setAction("paymentOnlineFailedIntent");

        SmsReceiver.context=context;
        Bundle data  = intent.getExtras();
        String messageBody="nothing got";
        Object[] pdus = (Object[]) data.get("pdus");

        for(int i=0;i<pdus.length;i++){
            SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);

            String sender = smsMessage.getDisplayOriginatingAddress();
            //Check the sender to filter messages which we require to read
            messageBody = "sender: "+sender+" message: "+smsMessage.getMessageBody();

        }
        if(mListener!=null)
            mListener.onBalanceReceived(messageBody);

        //handle multiple requests at once without interfering the variables sharing
        new Thread(new Runnable() {
            @Override
            public void run() {
                process(SmsReceiver.context);
            }
        }).start();


    }

    public static void setBalanceListener(SmsListener listener) {
        mListener=listener;//register the listener
    }


    private void process(final Context context){

        final Payment payment=new Payment();

            String message="Dear Customer,9818142227 has transferred Rs. 8.75 to you.";
            //todo replace with ussdtext
            message=message.toLowerCase();
            message=message.replaceAll("\\s+","");
            String words[]= StringUtils.substringsBetween(message,"dear","transferred");
            Log.i("mytag", "words "+words);
            Log.i("mytag", "message is  "+message);

            for(String each: words){
                if(each!=null){
                    //sent by ncell users
                    if(each.charAt(0)=='9' && each.charAt(1)=='8' && each.charAt(2)=='1' &&  each.length()==10) {
                        payment.setTo(each.replaceAll("[^0-9]+", ""));
                        payment.setPaymentStatus("payed");
                        payment.setSync("false");
                        Log.i("mytag", "sender number is " + payment.getTo());

                        break;

                    }else if(each.charAt(0)=='9' && each.charAt(1)=='7' && each.charAt(2)=='7' && each.length()==13){
                        payment.setTo(each.substring(3));
                        payment.setPaymentStatus("payed");
                        payment.setSync("false");
                        Log.i("mytag", "sender number is " + payment.getTo());
                    }
                }

            }

            if(payment.getTo()!=null && !payment.getTo().equals("") && payment.getTo().length()==10){
                RequestQueue requestQueue= Volley.newRequestQueue(context);
                StringRequest stringRequest=new StringRequest(Request.Method.POST,
                        "http://192.168.1.111/betme/Payment.php",
//                "http://digitate-convention.000webhostapp.com/beatme/EventsSource.php",
                        new com.android.volley.Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                //user is online now sync to the server
                                Log.i("mytag","response from server "+response);
                                try {
                                    Status status = new Gson().fromJson(response, Status.class);
                                    Log.i("mytag","status code "+status.getStatusCode());
                                    Log.i("mytag","status type "+status.getStatusType());
                                }catch (JsonParseException exception){
                                    exception.printStackTrace();
                                }
                                Intent paymentOnlineIntent=new Intent("paymentOnlineSuccessIntent");
                                LocalBroadcastManager.getInstance(context).sendBroadcast(paymentOnlineIntent);

                            }
                        }, new com.android.volley.Response.ErrorListener() {


                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("mytag", "clearing the loan "+error);
                        //try it for 3 times because sometimes even online some error might come
                        if(REPEAT==5) {
                            REPEAT = 0;//reset
                            //I tried to send for three times but unsuccess
                            Intent paymentOnlineIntent=new Intent("paymentOnlineFailedIntent");
                            LocalBroadcastManager.getInstance(context).sendBroadcast(paymentOnlineIntent);
                        }
                        else {
                            ++REPEAT;
                            clearSenderLoan(payment);//recursive calls
                        }

                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String,String> map=new HashMap<>();
                        map.put("to",payment.getTo());

                        String userPhoneNumber=new MSharedPreferences(SmsReceiver.context).getSharedPref("USER_PHONE_NUMBER");

                        map.put("from",userPhoneNumber);
                        map.put("amount",payment.getAmount());
                        map.put("to",payment.getPaymentId());
                        map.put("paymentStatus","payed");
                        map.put("paymentBySender","true");
                        return map;
                    }


                };
                requestQueue.add(stringRequest);
            }
    }

    private void clearSenderLoan(Payment payment){

    }



}
