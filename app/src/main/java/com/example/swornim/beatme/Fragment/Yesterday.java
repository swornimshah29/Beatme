package com.example.swornim.beatme.Fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.swornim.beatme.MSharedPreferences;
import com.example.swornim.musicnap.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Swornim on 10/26/2017.
 */

public class Yesterday extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(pendingBroadcastReceiver,new IntentFilter("pendingEvents"));
        runThread();

        View mView=inflater.inflate(R.layout.activity_register,container,false);
        return mView;

    }
    private BroadcastReceiver pendingBroadcastReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

//            Log.i("mytag","Broadcas called"+intent.getStringExtra("pendingEvents"));

        }
    };


    private void runThread(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                RequestQueue requestQueue= Volley.newRequestQueue(getContext());
                StringRequest stringRequest=new StringRequest(Request.Method.POST,
//                "http://digitate-convention.000webhostapp.com/beatme/Competion.php",
                        "http://192.168.1.104/bussinessLogic/user_product_events.php",
                        new com.android.volley.Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                               String mResult=response;
//
                                Intent intent=new Intent("pendingEvents");
                                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);

                            }
                        }, new com.android.volley.Response.ErrorListener() {


                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("mytag", "something went wrong User Points"+error);
                    }
                }){

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String,String> map=new HashMap<>();
                        map.put("hello","adsad");

                        return map;
                    }


                };
                requestQueue.add(stringRequest);

            }
        }).start();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(pendingBroadcastReceiver,new IntentFilter("pendingEvents"));

    }
}
