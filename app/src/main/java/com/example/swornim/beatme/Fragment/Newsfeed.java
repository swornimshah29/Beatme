package com.example.swornim.beatme.Fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.swornim.beatme.MSharedPreferences;
import com.example.swornim.beatme.MinorDetails;
import com.example.swornim.beatme.ServerSideTask;
import com.example.swornim.musicnap.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Swornim on 6/18/2017.
 */

public class Newsfeed extends Fragment {

    private ListView listView;
    private ArrayAdapter<MinorDetails> adapter ;
    private List<MinorDetails> nContainer=new ArrayList<>();



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        MinorDetails minorDetails=new MinorDetails();
        minorDetails.setFunctionCalledForOperation("newsfeed");
        new ServerSideTask(getActivity(),minorDetails).execute();

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mNewsfeedBroadcastReceiver,new IntentFilter("NewsfeedIntent"));

        View mView = inflater.inflate(R.layout.news_result, container, false);
        adapter=new NewsfeedAdapter(getActivity(),nContainer);
        listView=(ListView) mView.findViewById(R.id.news_result_id);
        listView.setAdapter(adapter);

        return mView;


    }



        private class NewsfeedAdapter extends ArrayAdapter<MinorDetails> {

            public NewsfeedAdapter(@NonNull Context context,List<MinorDetails> nContainer) {
                super(context, R.layout.news_result_template, nContainer);
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


                View mView=convertView;
                if(convertView==null) {
                    LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                    mView = layoutInflater.inflate(R.layout.news_result_template, parent, false);
                }

                Button leftLike=(Button) mView.findViewById(R.id.news_result_tem_left_like);
                Button rightLike=(Button) mView.findViewById(R.id.news_result_tem_rigbt_like);

                ImageView leftTeam=(ImageView)mView.findViewById(R.id.news_result_left_team);
                ImageView rightTeam=(ImageView)mView.findViewById(R.id.news_result_right_team);
                ImageView leftTick=(ImageView)mView.findViewById(R.id.leftTickId);
                ImageView rightTick=(ImageView)mView.findViewById(R.id.rightTickId);
                TextView userName1=(TextView) mView.findViewById(R.id.news_result_user_name1);
                TextView userName2=(TextView) mView.findViewById(R.id.news_result_user_name2);

                userName1.setText(nContainer.get(position).getUserName1());
                userName2.setText(nContainer.get(position).getUserName2());
                if(nContainer.get(position).getWinnerName().equals(nContainer.get(position).getUserName1()))
                    rightTick.setImageResource(0);
                else
                    leftTick.setImageResource(0);

                leftLike.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialogBox();
                    }
                });


                return mView;

            }
        }

        private BroadcastReceiver mNewsfeedBroadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String globalNewsfeed=new MSharedPreferences(getActivity()).getSharedPref("NewsfeedSharedpref");
                nContainer.clear();
                Log.i("mytag","broadcaster is called");
                //                process the json data and update the adapter for new  newsfeedtemplate
                try {
                    JSONObject root = new JSONObject(globalNewsfeed);
                    JSONArray mainArray=root.getJSONArray("result");
                    for(int i=0;i<mainArray.length();i++){
                        JSONObject eachObject=mainArray.getJSONObject(i);
                        MinorDetails minorDetails=new MinorDetails();
                        minorDetails.setUserName1(eachObject.getString("userName1"));
                        minorDetails.setUserName2(eachObject.getString("userName2"));
                        minorDetails.setUniqueId1(eachObject.getString("uniqueId1"));
                        minorDetails.setUniqueId2(eachObject.getString("uniqueId2"));
                        minorDetails.setTeamName1(eachObject.getString("teamName1"));
                        minorDetails.setTeamName2(eachObject.getString("teamName2"));
                        minorDetails.setWinnerName(eachObject.getString("winnerName"));

                        nContainer.add(minorDetails);

                    }
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Toast.makeText(getActivity(),globalNewsfeed,Toast.LENGTH_LONG).show();

            }
        };

    private void alertDialogBox(){

        Dialog dialog=new Dialog(getActivity());
        dialog.setTitle("Sub questions");
        dialog.setContentView(R.layout.scrollable);
        dialog.setCancelable(true);
        dialog.show();

    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mNewsfeedBroadcastReceiver);

    }
}

