package com.example.swornim.beatme;

import java.io.Serializable;

/**
 * Created by Swornim on 6/13/2017.
 */

public class Details implements Serializable {

    //gallery details
    private String photoName;
    private String photoType;
    private String photoUrl;
    private String photoCaption;
    private String photoDate;
    private String photoCategory;//screenshot, downloads etc
    private String photoPhoneUrl;//screenshot, downloads etc
    private String imageHeight;
    private String imageWidth;
    private String photoSize;


    public String getPhotoSize() {
        return photoSize;
    }

    public void setPhotoSize(String photoSize) {
        this.photoSize = photoSize;
    }

    public String getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(String imageHeight) {
        this.imageHeight = imageHeight;
    }

    public String getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(String imageWidth) {
        this.imageWidth = imageWidth;
    }

    public String getPhotoPhoneUrl() {
        return photoPhoneUrl;
    }

    public void setPhotoPhoneUrl(String photoPhoneUrl) {
        this.photoPhoneUrl = photoPhoneUrl;
    }

    public String getPhotoName() {
        return photoName;
    }

    public void setPhotoName(String photoName) {
        this.photoName = photoName;
    }

    public String getPhotoType() {
        return photoType;
    }

    public void setPhotoType(String photoType) {
        this.photoType = photoType;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPhotoCaption() {
        return photoCaption;
    }

    public void setPhotoCaption(String photoCaption) {
        this.photoCaption = photoCaption;
    }

    public String getPhotoDate() {
        return photoDate;
    }

    public void setPhotoDate(String photoDate) {
        this.photoDate = photoDate;
    }

    public String getPhotoCategory() {
        return photoCategory;
    }

    public void setPhotoCategory(String photoCategory) {
        this.photoCategory = photoCategory;
    }
}
