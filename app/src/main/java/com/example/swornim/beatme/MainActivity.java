package com.example.swornim.beatme;

import android.app.AlarmManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.swornim.beatme.Fragment.Newsfeed;
import com.example.swornim.beatme.Fragment.SideBarGlobalNewsfeed;
import com.example.swornim.beatme.Fragment.SideBarHomeFragment;
import com.example.swornim.beatme.Fragment.SideBarNotificationFragment;
import com.example.swornim.beatme.Fragment.SideBarPointsFragment;
import com.example.swornim.musicnap.Manifest;
import com.example.swornim.musicnap.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private NavigationView navigationView;
    private FragmentTransaction fragmentTransaction;
    private DrawerLayout mDrawerLayout;
    private TextView virtualBalance;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sidebarlayout);


            fragmentTransaction = getSupportFragmentManager().beginTransaction();

            fragmentTransaction.replace(R.id.mainContainer, new SideBarHomeFragment());
            fragmentTransaction.commit();
            getSupportActionBar().setTitle("Events");

            new FirebaseTask().deleteLoan();
            new FirebaseTask().insertLoan();


            //whenever the side bar is slided ,side bar is called navigation view

            navigationView = (NavigationView) findViewById(R.id.navigationViewId);
            navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.sideBarGlobalNewsfeed:
                            fragmentTransaction = getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.mainContainer, new SideBarGlobalNewsfeed());
                            fragmentTransaction.addToBackStack("global");
                            fragmentTransaction.commit();
                            getSupportActionBar().setTitle("Top Players");
                            break;

                        case R.id.sideBarNotification:
                            fragmentTransaction = getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.mainContainer, new SideBarNotificationFragment());

                            fragmentTransaction.commit();
                            getSupportActionBar().setTitle("Notification");
                            break;

                        case R.id.sideBarEvent:
                            fragmentTransaction = getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.mainContainer, new SideBarHomeFragment());

                            fragmentTransaction.commit();
                            getSupportActionBar().setTitle("Events");
                            break;

                        case R.id.Stars:
                            mDrawerLayout.closeDrawers();
                            fragmentTransaction = getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.mainContainer, new SideBarPointsFragment());
                            fragmentTransaction.commit();
                            getSupportActionBar().setTitle("Your Points");
                            break;

                        default:
                            //do nothing
                    }

                    return false;


                }
            });

//        ActionBar actionBar=getSupportActionBar();
//        actionBar.setLogo(R.drawable.ic_comment_3x);
//        actionBar.setDisplayUseLogoEnabled(true);
//        actionBar.setDisplayShowHomeEnabled(true);//this will add the icon on the action bar
//
            int[] array = {1, 2, 3, 4, 5, 6, 5, 62, 88, 200, 500, 782, 999, 1000, 1222};
            int data = 2;
            int low = 0, middle, high = array.length;


            while (true) {
                middle = low + (high - low) / 2;

                if (data == array[middle]) {
                    // Toast.makeText(getApplicationContext(),array.length+"Item found"+ ++middle,Toast.LENGTH_LONG).show();
                    break;
                } else if (data > array[middle]) {
                    //search right and ignore the lefts also increament middle to 1
                    low = middle + 1;
                    continue;
                } else if (data < array[middle]) {
                    //search left and ignore the rights also decrement middle to 1
                    high = middle - 1;
                    continue;
                } else {
                }

            }

            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();


            //to json string from class, for more use tutorials point.com
            MinorDetails minorDetails = new MinorDetails();
            minorDetails.setUserName("Swornim bikram shah");
            minorDetails.setRecgargeCardType("The card type is ncell");

            List<String> comments = new ArrayList<>();
            comments.add("Hello khatra goal hanyo haii");
            comments.add("Hi nice the game is khatra goal hanyo haii");
            comments.add("Bholi ko match ma herum laa aba");
            comments.add("Ajai baki cha ke cha ");

            minorDetails.setJsonComments(comments);


            String jsonString = gson.toJson(minorDetails);

            Log.i("mytag", new MSharedPreferences(getApplicationContext()).getSharedPref("uniqueUserId"));


            //to class
//        MinorDetails minorDetails=gson.fromJson(JSONSTRING,MinorDetails.class)

        }







    @Override
    protected void onStart() {
        //send request to server for the notifications of the previous beats
        super.onStart();
    }

    @Override
    public void onBackPressed() {

        finish();

    }

    @Override
    protected void onStop()
    {
        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 11: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

}





