package com.example.swornim.beatme;

import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.HttpAuthHandler;
import android.widget.ListView;

import com.example.swornim.beatme.Fragment.*;
import com.example.swornim.beatme.Fragment.Newsfeed;
import com.example.swornim.musicnap.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Swornim on 10/10/2017.
 */

public class Homefragment extends Fragment implements RewardedVideoAdListener   {
    private RewardedVideoAd mRewardedVideoAd;
    private InterstitialAd mInterstitialAd;
    private Runnable runnable;
    private Handler handler;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler=new Handler();
        runnable=new Runnable() {
            @Override
            public void run() {
                Log.i("mytag","running for 200ms");

            }
        };
        handler.postDelayed(runnable,200);


// Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View mView = inflater.inflate(R.layout.user_profile_page, container, false);
        return mView;

    }

    @Override
    public void onRewardedVideoAdLoaded() {
        Log.i("mytag","video has been loaded");
        if(mRewardedVideoAd.isLoaded())
        mRewardedVideoAd.show();

        handler.removeCallbacks(runnable);
    }

    @Override
    public void onRewardedVideoAdOpened() {
        Log.i("mytag","video has been opened");

    }

    @Override
    public void onRewardedVideoStarted() {
        Log.i("mytag","video has been started");

    }

    @Override
    public void onRewardedVideoAdClosed() {
        Log.i("mytag","video has been closed");
        mInterstitialAd = new InterstitialAd(getContext());
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
//        mInterstitialAd.loadAd(new AdRequest.Builder().build());


        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // Load the next interstitial.
//                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                if(mInterstitialAd.isLoaded())
                    mInterstitialAd.show();
            }
        });

    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        Log.i("mytag","user has been rewarded");

    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        Log.i("mytag","user has left the application");

    }




    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        Log.i("mytag","video failed to load");
        mRewardedVideoAd.loadAd("ca-app-pub-3940256099942544/5224354917",
                new AdRequest.Builder().build());

    }


    @Override
    public void onStart() {
        super.onStart();
//        MobileAds.initialize(getContext(), "ca-app-pub-3940256099942544~3347511713");
//        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(getActivity());
//        mRewardedVideoAd.setRewardedVideoAdListener(this);
//        mRewardedVideoAd.loadAd("ca-app-pub-3940256099942544/5224354917",
//                new AdRequest.Builder().build());
        Log.i("mytag","onstart home is called");

    }
}
