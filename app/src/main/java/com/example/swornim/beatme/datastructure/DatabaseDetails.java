package com.example.swornim.beatme.datastructure;

import com.example.swornim.beatme.Details;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class DatabaseDetails implements Serializable{
    /*This class represents almost all the variables used in the database*/

    //for sending phone numbers of friends in server
    private String friendNumber;
    private String friendName;
    private String hasApp;




    private String amount;
    private String eventId;
    private String leftTeamName;
    private String rightTeamName;
    private String opponentNumber;
    private String paymentStatus;//set by server to clients bets
    private String score;
    private String opponentsim;
    private String usersim;
    private String teamSelected;
    private String userNumber;
    private String timestamp;
    private String to;
    private String successBalanceSent;//when client sends via phone ussd is analaysised

    //profile
    private String lastSeen;
    private String dieHardFan;
    private String userName;
    private List<String> supporter;
    private String status;


    //create maps to hold profiles etc
    Map<String,DatabaseDetails> profile;

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public String getHasApp() {
        return hasApp;
    }

    public void setHasApp(String hasApp) {
        this.hasApp = hasApp;
    }

    public String getFriendNumber() {
        return friendNumber;
    }

    public void setFriendNumber(String friendNumber) {
        this.friendNumber = friendNumber;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getLeftTeamName() {
        return leftTeamName;
    }

    public void setLeftTeamName(String leftTeamName) {
        this.leftTeamName = leftTeamName;
    }

    public String getRightTeamName() {
        return rightTeamName;
    }

    public void setRightTeamName(String rightTeamName) {
        this.rightTeamName = rightTeamName;
    }

    public String getOpponentNumber() {
        return opponentNumber;
    }

    public void setOpponentNumber(String opponentNumber) {
        this.opponentNumber = opponentNumber;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getOpponentsim() {
        return opponentsim;
    }

    public void setOpponentsim(String opponentsim) {
        this.opponentsim = opponentsim;
    }

    public String getUsersim() {
        return usersim;
    }

    public void setUsersim(String usersim) {
        this.usersim = usersim;
    }

    public String getTeamSelected() {
        return teamSelected;
    }

    public void setTeamSelected(String teamSelected) {
        this.teamSelected = teamSelected;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSuccessBalanceSent() {
        return successBalanceSent;
    }

    public void setSuccessBalanceSent(String successBalanceSent) {
        this.successBalanceSent = successBalanceSent;
    }

    public String getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(String lastSeen) {
        this.lastSeen = lastSeen;
    }

    public String getDieHardFan() {
        return dieHardFan;
    }

    public void setDieHardFan(String dieHardFan) {
        this.dieHardFan = dieHardFan;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<String> getSupporter() {
        return supporter;
    }

    public void setSupporter(List<String> supporter) {
        this.supporter = supporter;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Map<String, DatabaseDetails> getProfile() {
        return profile;
    }

    public void setProfile(Map<String, DatabaseDetails> profile) {
        this.profile = profile;
    }
}
