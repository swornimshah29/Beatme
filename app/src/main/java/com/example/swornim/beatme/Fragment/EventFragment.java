package com.example.swornim.beatme.Fragment;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.swornim.beatme.MSharedPreferences;
import com.example.swornim.beatme.MinorDetails;
import com.example.swornim.beatme.RegisterActivity;
import com.example.swornim.beatme.ServerSideTask;
import com.example.swornim.beatme.datastructure.Events;
import com.example.swornim.beatme.datastructure.Status;
import com.example.swornim.beatme.tabactivity;
import com.example.swornim.musicnap.R;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by Swornim on 10/11/2017.
 */

public class EventFragment extends Fragment{

    private ArrayAdapter<Events.Epl> eplAdapter;
    private ListView neweventListView;
    private List<Events.Epl> eplList=new ArrayList<>();

    private List<Events.Epl> selectioneplList=new ArrayList<>();

    private ProgressBar eventProgressBarId;
    private Gson gson=new Gson();
    private Events publishEventObject;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(EventBroadcastReceiver,new IntentFilter("MyEvents"));

        MinorDetails minorDetails=new MinorDetails();
        minorDetails.setFunctionCalledForOperation("events");
        new ServerSideTask(getContext(),minorDetails).execute();
        //called everytime the app is opened but not during the foreground process i.e within the application
        //so kind of like caching

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View mView= inflater.inflate(R.layout.main_event_fragment,container,false);
        neweventListView=(ListView) mView.findViewById(R.id.neweventListView);
        eplAdapter = new EventFragmentAdapter(getContext(),eplList);
        neweventListView.setAdapter(eplAdapter);

        return mView;
    }

    private BroadcastReceiver EventBroadcastReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("mytag", "EVENT BROADCASTER Name"+intent.getAction() );

            String myEventsJsonString = new MSharedPreferences(getContext()).getSharedPref("eventJsonString");
            Log.i("mytag", "eventjsonstring intent "+myEventsJsonString );

            Gson gson = new Gson();
            if(!myEventsJsonString.equals("")) {
                Events eventsObject = gson.fromJson(myEventsJsonString, Events.class);
                if (eventsObject instanceof Events) {
                    refreshContent();
                    Log.i("mytag","yes it is the instance");
                }

            }
        }
    };


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.i("mytag", "on destroy event view called" );

    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.i("mytag", "on deattach event  called" );
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(EventBroadcastReceiver);


    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i("mytag", "on start event view called" );
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(EventBroadcastReceiver,new IntentFilter("intentEvent"));

        refreshContent();


    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("mytag", "on pause event view called" );

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.i("mytag", "on attach event is called " );
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(EventBroadcastReceiver,new IntentFilter("MyEvents"));


    }


    private void refreshContent(){
        boolean useCache=false;
        if(eplAdapter!=null && neweventListView!=null
                && !new MSharedPreferences(getContext()).getSharedPref("eventJsonString").equals("")
                && !new MSharedPreferences(getContext()).getSharedPref("eventJsonStringCache").equals("")
                ){

            //check if the server side response is same as the cache data
            Events cacheEvents=new Gson().fromJson(new MSharedPreferences(getContext()).getSharedPref("eventJsonStringCache"),Events.class);
            Events newEvents=new Gson().fromJson(new MSharedPreferences(getContext()).getSharedPref("eventJsonString"),Events.class);

            for(Events.Epl eachNew:newEvents.getEpl()){
                for(Events.Epl eachCache:cacheEvents.getEpl()){
                    if(eachNew.getEventId().equals(eachCache.getEventId())){
                        useCache=true;
                        Log.i("mytag","yes same id "+eachNew.getEventId());
                        break;
                    }
                }
                if(useCache){break;}//also break from the outerloop
            }
            if(useCache){
                eplList=cacheEvents.getEpl();
                eplAdapter=new EventFragmentAdapter(getContext(),eplList);
                neweventListView.setAdapter(eplAdapter);
                eplAdapter.notifyDataSetChanged();
            }else{
                eplList=newEvents.getEpl();
                eplAdapter=new EventFragmentAdapter(getContext(),eplList);
                neweventListView.setAdapter(eplAdapter);
                eplAdapter.notifyDataSetChanged();

            }

        }else if(!new MSharedPreferences(getContext()).getSharedPref("eventJsonString").equals("") && neweventListView!=null){
            //for the first time when cache data is not set
            Events newEvents=new Gson().fromJson(new MSharedPreferences(getContext()).getSharedPref("eventJsonString"),Events.class);
            eplList=newEvents.getEpl();
            eplAdapter=new EventFragmentAdapter(getContext(),eplList);
            neweventListView.setAdapter(eplAdapter);
            eplAdapter.notifyDataSetChanged();

        }

    }


    //Holder class to hold the references whenever the getview method gets called





    public class EventFragmentAdapter extends ArrayAdapter<Events.Epl> implements View.OnClickListener,View.OnLongClickListener{
        ImageView rightTeamImage;
        ImageView leftTeamSelected;
        ImageView rightTeamSelected;
        ImageView pendingEventIcon;
        TextView  pendingEventStatus;
        TextView  leftTeamName;
        TextView  rightTeamName;
        ImageView leftTeamImage;
        private int position;


        public EventFragmentAdapter(Context context,List<Events.Epl> eplList) {
            super(context,R.layout.main_event_fragment_template,eplList);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            this.position=position;
            View mView = convertView;
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(getActivity());
                mView = inflater.inflate(R.layout.main_event_fragment_template, parent, false);
            }

            rightTeamImage = (ImageView) mView.findViewById(R.id.rightImage);
            leftTeamImage = (ImageView) mView.findViewById(R.id.leftImage);
            rightTeamSelected = (ImageView) mView.findViewById(R.id.rightTeamSelected);
            leftTeamSelected = (ImageView) mView.findViewById(R.id.leftTeamSelected);
            pendingEventIcon = (ImageView) mView.findViewById(R.id.pendingEventIcon);
            pendingEventStatus = (TextView) mView.findViewById(R.id.pendingEventStatus);
            leftTeamName = (TextView) mView.findViewById(R.id.leftTeamName);
            rightTeamName = (TextView) mView.findViewById(R.id.rightTeamName);
            pendingEventStatus = (TextView) mView.findViewById(R.id.pendingEventStatus);
//            rightTeamImage.setImageResource(R.drawable.arsenal);
//            leftTeamImage.setImageResource(R.drawable.chelsea);
//            leftTeamName.setText(eplList.get(position).getLeftTeamName());
            rightTeamName.setText(eplList.get(position).getAmount());
            leftTeamName.setText(eplList.get(position).getAmount());


            rightTeamImage.setOnClickListener(this);
            leftTeamImage.setOnClickListener(this);
            rightTeamImage.setOnLongClickListener(this);
            leftTeamImage.setOnLongClickListener(this);

            leftTeamImage.setTag(position);
            rightTeamImage.setTag(position);



            if(eplList.get(position).getTick().equals("left")){
                leftTeamSelected.setImageResource(R.drawable.tick);
                rightTeamSelected.setImageResource(0);
            }else if (eplList.get(position).getTick().equals("right")){
                rightTeamSelected.setImageResource(R.drawable.tick);
                leftTeamSelected.setImageResource(0);
            }else{
                rightTeamSelected.setImageResource(0);
                leftTeamSelected.setImageResource(0);
            }


            return mView;

        }

        @Override
        public void onClick(View view) {
            Log.i("mytag","left image clicked inside");

            switch (view.getId()){
//
//                case R.id.rightImage:
//                    Log.i("mytag","right image clicked ");
//                    leftTeamSelected.setImageResource(0);
//                    rightTeamSelected.setImageResource(R.drawable.tick);
//                    eplList.get(position).setTick("right");
//                    updateCache();
//                    break;
//
//                case R.id.leftImage:
//                    Log.i("mytag","left image clicked ");
//                    rightTeamSelected.setImageResource(0);
//                    leftTeamSelected.setImageResource(R.drawable.tick);
//                    eplList.get(position).setTick("left");
//                    updateCache();
//
//                    break;

            }

        }

        @Override
        public boolean onLongClick(View view) {

            switch (view.getId()){
                case R.id.leftImage:
                    Log.i("mytag",view.getTag()+"selected");
                    teamConfirmationDialog(eplList.get((int)view.getTag()).getLeftTeamName(),(int)view.getTag(),"left");
                    break;

                    case R.id.rightImage:
                    Log.i("mytag",view.getTag()+"selected");
                        teamConfirmationDialog(eplList.get((int)view.getTag()).getRightTeamName(),(int)view.getTag(),"right");
                    break;
            }

            return false;
        }
    }


    @Override
    public void onResume() {

        super.onResume();
        Log.i("mytag", "on resume view called" );

    }

    private void teamConfirmationDialog(final String whichTeamSelected, final int whichPosition, final String whichSide ){
        final AlertDialog.Builder builder=new AlertDialog.Builder(getActivity(),R.style.Theme_AppCompat_Dialog);
        builder.setTitle("Do you wanna go with "+whichTeamSelected  );
        builder.setCancelable(false);
        builder.setPositiveButton("yup", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int which) {
                dialog.dismiss();
                final Dialog loadingDialog = new Dialog(getContext());
                loadingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                loadingDialog.setContentView(R.layout.loading);
                loadingDialog.setCancelable(false);
                loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                loadingDialog.show();

                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        RequestQueue requestQueue= Volley.newRequestQueue(getContext());
                        StringRequest stringRequest=new StringRequest(Request.Method.POST,
//                "http://digitate-convention.000webhostapp.com/beatme/Competion.php",
                                "http://192.168.1.111/betme/betting.php",
                                new com.android.volley.Response.Listener<String>() {
                                    @Override
                                    public void onResponse(final String response) {


                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                Gson gson=new Gson();
                                                try {

                                                    Status status = gson.fromJson(response, Status.class);

                                                    if (status.getStatusType().equals("betting") && status.getStatusCode().equals("success")) {
                                                        Log.i("mytag", "valid json");
                                                        Toast.makeText(getContext(),"Successfully betted",Toast.LENGTH_LONG).show();
                                                        eplList.get(whichPosition).setTick(whichSide);
                                                        updateCache();
                                                        Log.i("mytag","eventJson selected is "+new Gson().toJson(eplList.get(whichPosition)));

                                                    }
                                                    else if(status.getStatusType().equals("betting") && status.getStatusCode().equals("already")){
                                                        Toast.makeText(getContext(),"You already had a bet for this event",Toast.LENGTH_LONG).show();
                                                    }
                                                    else if(status.getStatusType().equals("betting") && status.getStatusCode().equals("failed")){
                                                        Toast.makeText(getContext(),"Server side error",Toast.LENGTH_LONG).show();
                                                    }


                                                }catch(JsonSyntaxException jse){
                                                    Log.i("mytag","invalid json from the server");
                                                    Toast.makeText(getContext(),"Invalid json response from the server",Toast.LENGTH_LONG).show();
                                                }

                                                loadingDialog.dismiss();//dismiss after 1.5 seconds of reponse from the server

                                            }
                                        }, 1500);
                                    }
                                }, new com.android.volley.Response.ErrorListener() {


                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.i("mytag", "something went wrong User Points"+error);
                                Toast.makeText(getContext(),"Error try again",Toast.LENGTH_LONG).show();
                                loadingDialog.dismiss();

                            }
                        }){

                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {

                                Map<String,String> map=new HashMap<>();
                                map.put("eventJson",new Gson().toJson(eplList.get(whichPosition)));
                                map.put("userPhoneNumber","9813847444");
                                map.put("eventId",eplList.get(whichPosition).getEventId());

                                return map;
                            }


                        };
                        requestQueue.add(stringRequest);

                    }
                }).start();

            }

        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });

        AlertDialog alertDialog=builder.create();
        alertDialog.show();

    }




    private void updateCache(){
        Gson gson=new Gson();
        Events eventsObject=new Events();
        eventsObject.setEpl(eplList);
        new MSharedPreferences(getContext()).setSharedPref("eventJsonStringCache", gson.toJson(eventsObject,Events.class));
        eplAdapter.notifyDataSetChanged();
    }





}
