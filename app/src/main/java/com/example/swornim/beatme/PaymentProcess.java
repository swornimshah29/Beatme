package com.example.swornim.beatme;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.CursorIndexOutOfBoundsException;
import android.net.Uri;
import android.provider.Telephony;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.swornim.beatme.Interface.SmsListener;
import com.example.swornim.beatme.datastructure.Payment;
import com.example.swornim.beatme.datastructure.Status;
import com.example.swornim.musicnap.R;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.accessibilityservice.AccessibilityService.GLOBAL_ACTION_BACK;

public class PaymentProcess extends AppCompatActivity {


    private ArrayAdapter<Payment> adapter;
    private ListView mListView;
    private List<Payment> payments = new ArrayList<>();//if not initialize error
    private static String CHECK_BALANCE_USSD = " 901";
    private static String SEND_BALANCE_USSD = " 17122";
    private static String PHONE_USSD_CODE = " 903";
    private  int CURRENT_BALANCE;
    private  int CURRENT_LOAN_USER_INDEX;
    private  int EXPECTED_BALANCE;
    private  int CURRENT_PAYING_BALANCE;
    private int counter=0;
    private Button balance,phone,sendBalance;
    private static int CURRENT_PAYING_USER_POSITION=-1;//-1 means null
    private static  int REPEAT=0;//to call again if failed

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hasLoans();
        IntentFilter ussdIntentFilter=new IntentFilter();
        ussdIntentFilter.addAction("phoneNumberIntent");//phone number verification
        ussdIntentFilter.addAction("balanceIntent");//balance check intent

        IntentFilter paymentIntent=new IntentFilter();
        paymentIntent.addAction("paymentOnlineFailedIntent");
        paymentIntent.addAction("sendBalanceIntent");
        paymentIntent.addAction("paymentOnlineSuccessIntent");

        //register the listener interface
        SmsReceiver.setBalanceListener(new SmsListener() {
            @Override
            public void onBalanceReceived(String message) {
                Log.i("mytag","message is "+message);
            }
        });

        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(UssdBroadCastListener,ussdIntentFilter);
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(PaymentBroadCastListener,paymentIntent);

        setContentView(R.layout.activity_payment_process);
        adapter = new PaymentAdapter(getApplicationContext(), payments);
        mListView = (ListView) findViewById(R.id.custPaymentListViewId);
        mListView.setAdapter(adapter);

        MinorDetails minorDetails = new MinorDetails();
        minorDetails.setFunctionCalledForOperation("Loans");
        new ServerSideTask(getApplicationContext(), minorDetails).execute();

        phone=(Button)findViewById(R.id.phone);
        balance=(Button)findViewById(R.id.balance);
        sendBalance=(Button)findViewById(R.id.sendBalance);

        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                USSDListener.whichUSSD="phoneNumberIntent";
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "*"+ PHONE_USSD_CODE+Uri.encode("#"))));
            }
        });

        balance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                USSDListener.whichUSSD="balanceIntent";
               Intent intent=new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "*"+ CHECK_BALANCE_USSD+Uri.encode("#")));
               startActivity(intent);
            }
        });

        sendBalance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                USSDListener.whichUSSD="sendBalanceIntent";
                Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "*"+ SEND_BALANCE_USSD+"*"+"9813847444"+"*"+"10"+Uri.encode("#")));
                startActivity(intent);
            }
        });


    }


    public class PaymentAdapter extends ArrayAdapter<Payment> {

        private List<MinorDetails> messageObject = new ArrayList<>();

        public PaymentAdapter(Context context, List<Payment> payments) {
            super(context, R.layout.custompayment, payments);
            this.messageObject = messageObject;
        }

        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {
            View mView = convertView;
            if (mView == null) {
                LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                mView = layoutInflater.inflate(R.layout.custompayment, parent, false);
            }

            //references
            TextView custpaymentUserName = (TextView) mView.findViewById(R.id.custpaymentUserName);
            TextView custpaymentAmount = (TextView) mView.findViewById(R.id.custpaymentAmount);
            Button custpaymentButton = (Button) mView.findViewById(R.id.custpaymentButton);

            custpaymentUserName.setText(payments.get(position).getTo());//name of receiver to pay for
            custpaymentAmount.setText(payments.get(position).getAmount());//name of receiver to pay for
            custpaymentButton.setTag(position);//assign id for each row

            custpaymentButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                  //  CURRENT_PAYING_BALANCE=array(position) get the balance from that listview
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }

                    //insert to database dynamically
                    new MSQLiteDatabase(getApplicationContext(),payments.get((int)view.getTag())).insertLoan();

                    CURRENT_PAYING_USER_POSITION=(int)view.getTag();
                    String USSD="*"+ CHECK_BALANCE_USSD+Uri.encode("#");
                    USSDListener.whichUSSD="sendBalanceIntent";
                    startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + USSD)));
                }
            });


            return mView;
        }
    }


    private BroadcastReceiver UssdBroadCastListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent!=null){
                if(intent.getAction().equals("phoneNumberIntent")){
                    String phoneNumber=intent.getStringExtra("phoneNumberIntent");
                    Log.i("mytag","phonenumber is "+phoneNumber);


                }else if(intent.getAction().equals("balanceIntent")){
                    String balance=intent.getStringExtra("balanceIntent");
                    if(balance!=null && !balance.equals("")){

                        CURRENT_BALANCE=Integer.parseInt(balance);
                        EXPECTED_BALANCE=CURRENT_BALANCE-12;
                        Log.i("mytag","current balance is "+CURRENT_BALANCE);
                        Log.i("mytag","expected balance is "+EXPECTED_BALANCE);
                        Log.i("mytag","counter  "+ ++counter);
                        Toast.makeText(getApplicationContext(),"Ussd is called "+counter,Toast.LENGTH_LONG).show();
                        //TODO: add some amount like 1 rupees because balance+some is needed to sent balance

                        if(CURRENT_BALANCE<12){
                            //show how much data is more needed in the UI of that listview item(postion)
                            //CB-CPB
                        }else{


                        }

                    }else{
                        //try again
                    }

                    //this means USSD sent was successfully done
                }else if(intent.getAction().equals("balanceSentIntent")){
                    //check the expected balance as well
                    //if success then processed to the

                }
            }

        }
    };


    //it only runs when success USSD of send balance
    private BroadcastReceiver PaymentBroadCastListener=new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent!=null) {
                Payment payment=payments.get(CURRENT_PAYING_USER_POSITION);
                String status;

                if(intent.getAction().equals("paymentOnlineSuccessIntent")){
                    //todo update the loan as synched:true and status:payed to make sure that next time he doesnot have to pay again
                    payment.setSync("true");
                    new MSQLiteDatabase(getApplicationContext(),payment).updateNSynchronize();
//                    new MSQLiteDatabase(getApplicationContext(),payment).readlAll();
                    Toast.makeText(getApplicationContext(), "Loan is cleared from server !! ", Toast.LENGTH_SHORT).show();

                    //todo check response later

                }else if(intent.getAction().equals("paymentOnlineFailedIntent")){
                    //Toast failed to send data but you have cleared the loan(update UI)
                    Toast.makeText(getApplicationContext(), "Loan is cleared from phone sync is needed !! ", Toast.LENGTH_SHORT).show();

                }else if(intent.getAction().equals("sendBalanceIntent")){
                    //save to db first then update the server
                    payment.setPaymentStatus("payed");
                    new MSQLiteDatabase(getApplicationContext(),payment).updateNSynchronize();
//                    new MSQLiteDatabase(getApplicationContext(),payment).readlAll();
                    clearLoan(payment);

                }

            }

        }
    };




    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

//
    private void startPaymentProcess(){

        //check online
        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET,
                "http://192.168.1.111/beatme/online.php",
//                "http://digitate-convention.000webhostapp.com/beatme/EventsSource.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //user is online now sync to the server
                        Intent paymentOnlineIntent=new Intent("paymentOnlineIntent");
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(paymentOnlineIntent);

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("mytag", "something went wrong during the fetch process"+error);
                //try it for 3 times because sometimes even online some error might come
                if(REPEAT==3) {
                    REPEAT = 0;//reset
                    Intent paymentOfflineIntent=new Intent("paymentOfflineIntent");
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(paymentOfflineIntent);
                }
                else {
                    ++REPEAT;
                    startPaymentProcess();//recursive calls
                }

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                map.put("online","online");

                return map;
            }

        };
        requestQueue.add(stringRequest);

    }



    private void clearLoan( Payment payment){
        final Payment paymentObject=payment;
        Log.i("mytag","cleared id is "+payment.getPaymentId());
        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                "http://192.168.1.111/betme/Payment.php",
//                "http://digitate-convention.000webhostapp.com/beatme/EventsSource.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //user is online now sync to the server
                        Log.i("mytag","response from server "+response);
                        try {
                            Status status = new Gson().fromJson(response, Status.class);
                            Log.i("mytag","status code "+status.getStatusCode());
                            Log.i("mytag","status type "+status.getStatusType());
                        }catch (JsonParseException exception){
                            exception.printStackTrace();
                        }
                        Intent paymentOnlineIntent=new Intent("paymentOnlineSuccessIntent");
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(paymentOnlineIntent);

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("mytag", "clearing the loan "+error);
                //try it for 3 times because sometimes even online some error might come
                if(REPEAT==5) {
                    REPEAT = 0;//reset
                    //I tried to send for three times but unsuccess
                    Intent paymentOnlineIntent=new Intent("paymentOnlineFailedIntent");
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(paymentOnlineIntent);
                }
                else {
                    ++REPEAT;
                    clearLoan(paymentObject);//recursive calls
                }

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                map.put("to",paymentObject.getTo());
                map.put("from",paymentObject.getFrom());
                map.put("amount",paymentObject.getAmount());
                map.put("paymentId",paymentObject.getPaymentId());
                map.put("paymentStatus",paymentObject.getPaymentStatus());
                map.put("paymentBySender","true");
                return map;
            }


        };
        requestQueue.add(stringRequest);


    }



    private void hasLoans(){


        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                "http://192.168.1.111/betme/Payment.php",
//                "http://digitate-convention.000webhostapp.com/beatme/EventsSource.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("mytag", "loans "+response);

                        try{
                            Payment.Loan loan=new Gson().fromJson(response,Payment.Loan.class);
                            Log.i("mytag", "loans "+loan);
                            if(loan!=null){
                                if(loan.getLoans().size()>0){
                                    for(Payment each:loan.getLoans()){
                                        Log.i("mytag","loans "+each.getAmount());
                                        Log.i("mytag","loans "+each.getPaymentStatus());
                                        Log.i("mytag","loans "+each.getTo());
                                        Log.i("mytag","loans "+each.getFrom());
                                        Log.i("mytag","loans "+each.getPaymentId());
                                        payments=loan.getLoans();
                                        adapter=new PaymentAdapter(getApplicationContext(),payments);
                                        mListView.setAdapter(adapter);
                                        //notify the listview
                                    }
                                }
                            }

                        }catch (JsonParseException e){
                            e.printStackTrace();
                        }


                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("mytag", "clearing the loan "+error);
                //try it for 3 times because sometimes even online some error might come
                if(REPEAT==4) {
                    REPEAT = 0;//reset
                }
                else {
                    ++REPEAT;
                    hasLoans();//recursive calls
                }

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                map.put("from","9813847444");
                map.put("hasLoan","true");

                return map;
            }


        };
        requestQueue.add(stringRequest);
    }








}
