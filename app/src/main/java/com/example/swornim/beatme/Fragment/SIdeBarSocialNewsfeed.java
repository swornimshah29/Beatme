package com.example.swornim.beatme.Fragment;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.swornim.beatme.GlobalNewsfeed;
import com.example.swornim.beatme.MSharedPreferences;
import com.example.swornim.beatme.MinorDetails;
import com.example.swornim.beatme.ServerSideTask;
import com.example.swornim.musicnap.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 *This class is the actual newsfeed of friends of friends
 */

public class SIdeBarSocialNewsfeed extends Fragment {

    private ListView globalTableListView;
    private ArrayAdapter<MinorDetails> adapter;
    private List<MinorDetails> Newsfeedpost=new ArrayList<>();//should always be intialized even data is null

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(SocialNewsfeedBroadcastReceiver,new IntentFilter("SocialNewsfeedIntent"));
        View mView = inflater.inflate(R.layout.fragment_side_bar_global_newsfeed, container, false);

        globalTableListView=(ListView) mView.findViewById(R.id.globalTableListView);
        adapter = new SocialNewsFeed(getContext(), Newsfeedpost);

        globalTableListView.setAdapter(adapter);

        MinorDetails minorDetails = new MinorDetails();
        minorDetails.setFunctionCalledForOperation("SocialNewsFeed");
        new ServerSideTask(getContext(), minorDetails).execute();

        return mView;

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(SocialNewsfeedBroadcastReceiver);

    }

    private BroadcastReceiver SocialNewsfeedBroadcastReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String globalNewsfeed=new MSharedPreferences(getActivity()).getSharedPref("SocialNewsFeed");
            Newsfeedpost.clear();
            //process the json data and update the adapter for new  newsfeedtemplate
            try {
                JSONArray root = new JSONArray(globalNewsfeed);
                for(int i=0;i<root.length();i++){
                    JSONArray innerArray=root.getJSONArray(i);
                    //loop inside the loop
                    for(int i1=0;i1<innerArray.length();i1++){
                        //render the json and populate the newsfeed


                    }
                }
                adapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };

    public class SocialNewsFeed extends ArrayAdapter<MinorDetails> {
        private Context mContext;
        private List<MinorDetails> postObjects = new ArrayList<>();


        public SocialNewsFeed(Context context, List<MinorDetails> postObjects) {
            super(context, R.layout.sidebarsocialnewsfeed_template, postObjects);
            mContext = context;
            this.postObjects = postObjects;
        }


        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {
            View mView = convertView;
            if (convertView == null) {
                LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                mView = layoutInflater.inflate(R.layout.sidebarsocialnewsfeed_template, parent, false);
            }

            return mView;

        }


    }

}
