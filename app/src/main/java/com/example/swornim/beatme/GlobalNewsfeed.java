package com.example.swornim.beatme;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.swornim.musicnap.R;

import java.util.List;

/**
 * Created by Swornim on 4/28/2017.
 */

public class GlobalNewsfeed extends ArrayAdapter<MinorDetails> {

    private List<MinorDetails> postObjects;

    public GlobalNewsfeed(Context context, List<MinorDetails> postObjects) {
        super(context, R.layout.globalnewstabletemplate,postObjects);
        this.postObjects=postObjects;
    }



    @Override
    public View getView(int position,  View convertView,  ViewGroup parent) {
        View mView=convertView;
        if(convertView==null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            mView = layoutInflater.inflate(R.layout.globalnewstabletemplate, parent, false);
        }

        ImageView follows=(ImageView) mView.findViewById(R.id.globalNewsfeedFollows);//favourite team icon

        TextView globalNewsfeedScore=(TextView) mView.findViewById(R.id.globalNewsfeedScore);
        TextView globalNewsfeedBetLost=(TextView) mView.findViewById(R.id.globalNewsfeedBetLost);
        TextView globalNewsfeedUserName=(TextView) mView.findViewById(R.id.globalNewsfeedUserName);

        globalNewsfeedUserName.setText(postObjects.get(position).getOtherUserNames());
        globalNewsfeedScore.setText(postObjects.get(position).getScore());
        globalNewsfeedBetLost.setText(postObjects.get(position).getBetLost());

        switch (postObjects.get(position).getFollows()) {
            case "liverpool":
                follows.setImageResource(R.drawable.liverpool);
                break;
            case "manu":
                follows.setImageResource(R.drawable.manu);
                break;
            case "chealsea":
                follows.setImageResource(R.drawable.chelsea);
                break;
            case "arsenal":
                follows.setImageResource(R.drawable.arsenal);
                break;

            case "barcelona":
                follows.setImageResource(R.drawable.mancity);
                break;

            case "Chealsea":
                follows.setImageResource(R.drawable.chelsea);
                break;

        }


        return mView;
    }
}
