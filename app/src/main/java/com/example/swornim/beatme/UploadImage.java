package com.example.swornim.beatme;

/**
 * Created by Swornim on 10/13/2017.
 */

import android.app.NotificationManager;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.swornim.musicnap.R;

import android.app.NotificationManager;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.swornim.musicnap.R;

/**
 * Created by Swornim on 10/13/2017.
 */

public class UploadImage extends AsyncTask<Void,Integer,String> {

    private Context context;

    public UploadImage(Context context){
        this.context=context;
    }

    NotificationManager mNotificationManager;
    NotificationCompat.Builder mNotificationBuilder;

    @Override
    protected String doInBackground(Void... voids) {
        for(int i=0;i<100;i++){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            publishProgress(i);
            Log.i("mytag","number"+i);
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        mNotificationManager=(NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        mNotificationBuilder=new NotificationCompat.Builder(context);
        mNotificationBuilder.setSmallIcon(R.mipmap.ic_notifications_black_48dp)
                .setContentTitle("Uploading Photo Please Wait")
                .setContentText("Please wait")
                .build();
        mNotificationManager.notify(100,mNotificationBuilder.build());
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
//            new Notification(getApplicationContext()).update(values[0]);
        mNotificationBuilder.setProgress(100,values[0],false);
        mNotificationBuilder.setContentText(values[0].toString()+"%");
        mNotificationManager.notify(100,mNotificationBuilder.build());

        super.onProgressUpdate(values);
    }
}

