package com.example.swornim.beatme.Fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.swornim.beatme.MinorDetails;
import com.example.swornim.beatme.ServerSideTask;
import com.example.swornim.musicnap.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Swornim on 10/30/2017.
 */

public class NotificationFragment extends Fragment {
    private ListView nlistView;
    Handler handler=new Handler();
    Runnable runnable;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadCastReceiver,new IntentFilter("notificationIntent"));
        MinorDetails minorDetails=new MinorDetails();
        minorDetails.setFunctionCalledForOperation("notificationLoad");
        new ServerSideTask(getContext(),minorDetails).execute();

    }




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.notificationlistview,container,false);
        nlistView=(ListView) view.findViewById(R.id.notificationListViewId);


        return  view;
    }




    private BroadcastReceiver mBroadCastReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("mytag", "broadcaster called" +intent.getStringExtra("MyNotificationJson") );

//            //process the json data and update the adapter for new  newsfeedtemplate
//            Gson gson = new Gson();
//            Events.Notification eventsObject = gson.fromJson(myEventsJsonString, Events.Notification.class);
//            if (eventsObject instanceof Events.Notification) {
////                    eventProgressBarId.setVisibility(View.GONE);
//
//            }

        }
    };



}
