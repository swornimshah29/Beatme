package com.example.swornim.beatme;

import android.app.*;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.swornim.musicnap.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by Swornim on 10/14/2017.
 */

public class PushNotificationService extends FirebaseMessagingService {
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mNotificationBuilder;
    private android.app.Notification notification;
    private PendingIntent pendingIntent;

    public PushNotificationService() {
        super();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.i("mytag",remoteMessage.getNotification().getBody());
        Log.i("mytag",remoteMessage.getNotification().getTitle());
        String title=remoteMessage.getNotification().getTitle();
        String message=remoteMessage.getNotification().getBody();

        mNotificationManager=(NotificationManager) getApplicationContext().getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
        mNotificationBuilder=new NotificationCompat.Builder(getApplicationContext());

        mNotificationBuilder.setSmallIcon(R.mipmap.ic_notifications_black_48dp)
                .setWhen(5)
                .setContentTitle(title)
                .setContentInfo("3 seconds ago")
                .setContentText(message);
        notification= mNotificationBuilder.build();
        mNotificationManager.notify(100,notification);


    }
}
