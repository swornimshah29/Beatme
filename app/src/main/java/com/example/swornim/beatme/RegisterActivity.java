package com.example.swornim.beatme;

import android.app.TabActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.swornim.beatme.datastructure.Status;
import com.example.swornim.musicnap.R;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
//import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.security.auth.callback.CallbackHandler;

public class RegisterActivity extends AppCompatActivity {


    private FirebaseAuth firebaseAuth;
    private SignInButton signInButton;
    private Button verifyPhonenumberbtn;
    private GoogleApiClient mGoogleApiClient;
    private FirebaseAuth.AuthStateListener authStateListener;
    private static int RC_SIGN_IN=9001;
    private ProgressBar signInProgressBar;
    private TextView signInProgressBarStatus;
    private MinorDetails userCredentials;
    private EditText userPhonenumber;
    private String mVerificationId;
    private String userPhoneNumberHolder;//hold the typed number to save in database
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private AlertDialog alertDialog=null;
    private String mDatabaseResult;//result status code from database



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String hasNumber=new MSharedPreferences(getApplicationContext()).getSharedPref("userPhoneNumber");

        if(hasNumber!="") {
            Toast.makeText(getApplicationContext(), "Phone number is valid", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(RegisterActivity.this, tabactivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        setContentView(R.layout.activity_register);
        //get the valid number verification

        signInProgressBarStatus = (TextView) findViewById(R.id.signInProgressBarStatus);
        signInProgressBar = (ProgressBar) findViewById(R.id.signInProgressBar);
        userPhonenumber = (EditText) findViewById(R.id.registerPhoneNumber);
        signInProgressBar.setVisibility(View.GONE);//by default dont show

        signInButton = (SignInButton) findViewById(R.id.signInButton);
        verifyPhonenumberbtn = (Button) findViewById(R.id.verfiyphoneButton);
        signInButton.setSize(SignInButton.SIZE_STANDARD);

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verificaiton without
                //     user action.


                    signInWithPhoneAuthCredential(phoneAuthCredential);

            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Toast.makeText(getApplicationContext(),"verification failed",Toast.LENGTH_LONG).show();
                Log.i("mytag","Verification failed");

            }

            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                //super.onCodeSent(s,forceResendingToken);
                mVerificationId = s;
                mResendToken = forceResendingToken;
                Toast.makeText(getApplicationContext(),"Token "+mResendToken+" and verification id is "+mVerificationId,Toast.LENGTH_LONG).show();
                Log.i("mytag","verification id is "+s+" toekn id is mVerificationId"+mResendToken);

                final AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                builder.setTitle("Enter the sms code");
                builder.setCancelable(false);//to avoid the closure of the dialog box when the user goes and sees the message
                builder.setView(R.layout.numberpad_template);
                builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText smsedittext=(EditText) alertDialog.findViewById(R.id.smsedittext);
                        if(smsedittext.getText()!=null && !smsedittext.getText().equals("")){

                            PhoneAuthCredential phoneAuthCredential=PhoneAuthProvider.getCredential(mVerificationId,smsedittext.getText().toString());
                            signInWithPhoneAuthCredential(phoneAuthCredential);

                        }else{
                            alertDialog.show();
                        }


                    }
                });

                builder.setNegativeButton("Resend", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alertDialog=builder.create();
                alertDialog.show();


            }


        };


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))

                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Log.i("mytag","connection failed google signin "+connectionResult);
                    }
                } /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        //call fast as possible
        //to avoid calling next time for registration
        String registeredOrNot=new MSharedPreferences(getApplicationContext()).getSharedPref("registeredOrNot");
        if(registeredOrNot.equals("already") ){
            startActivity(new Intent(RegisterActivity.this,MainActivity.class));
            finish();
        }

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                shouldISignIn();
            }
        });
        verifyPhonenumberbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               //code to call sms verfication goes here
                userPhoneNumberHolder=userPhonenumber.getText().toString();
                if(userPhonenumber.getText()!=null && !userPhonenumber.getText().equals("")){

                if(!validateUserRegisteration(String.valueOf(userPhonenumber.getText()))){
                    Toast.makeText(getApplicationContext(),"Enter valid number ",Toast.LENGTH_LONG).show();

                }else {

                    signInProgressBar.setVisibility(View.VISIBLE);
                    signInProgressBarStatus.setText("Please wait");

                    String phonenumber = userPhonenumber.getText().toString();
                    long seconds = 60;
                    PhoneAuthProvider.getInstance().verifyPhoneNumber(phonenumber, seconds, TimeUnit.SECONDS,
                            RegisterActivity.this, mCallbacks);
                }
            }else{
                    Toast.makeText(getApplicationContext(),"Enter valid number ",Toast.LENGTH_LONG).show();

                }

            }

        });

    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        FirebaseAuth.getInstance().signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mRegistrationBroadCast,new IntentFilter("registrationIntent"));

                            signInProgressBar.setVisibility(View.INVISIBLE);
                            signInProgressBarStatus.setText("Success");

                            new MSharedPreferences(getApplicationContext()).setSharedPref("userPhoneNumber",userPhoneNumberHolder);
                            PhoneNumberRegistration(userPhoneNumberHolder);

                            // ...
                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.i("mytag","phone number is invalid");
                            Toast.makeText(getApplicationContext(),"Phone number is invalid Try again",Toast.LENGTH_LONG).show();

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                            }
                        }
                    }
                });
    }

    private void shouldISignIn() {
        firebaseAuth=FirebaseAuth.getInstance();
        firebaseAuth.addAuthStateListener(new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {
                    Log.i("mytag", "user is signed in");
                    Intent intent=new Intent(RegisterActivity.this,tabactivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                }else{
                    Log.i("mytag","user has not signed in");
                    LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mRegistrationBroadCast,new IntentFilter("registrationIntent"));

                    signIn();
                }
            }
        });

    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        signInProgressBar.setVisibility(View.VISIBLE);
        signInProgressBarStatus.setText("please wait signing in....");

        startActivityForResult(signInIntent, RC_SIGN_IN);
        Log.i("mytag","startactivity called");

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("mytag","onacttivity result  called");

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            signInProgressBarStatus.setText("please wait signing in....");

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                //after the button account has been clicked show progresss bar// Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                userCredentials=new MinorDetails();
                userCredentials.setUserName(account.getEmail());
                userCredentials.setUserUniqueId(account.getId());
                firebaseAuthWithGoogle(account,userCredentials);

                //TODO User email account can change but the Id wont change so use id as uniqueId
                new MSharedPreferences(getApplicationContext()).setSharedPref("uniqueUserId",result.getSignInAccount().getId());
                new MSharedPreferences(getApplicationContext()).setSharedPref("username",result.getSignInAccount().getId());
                Log.i("mytag ",result.getSignInAccount().getEmail());
                Log.i("mytag ",result.getSignInAccount().getId());

            } else {
                Log.i("mytag","google sign in failed to authenticate");

                // Google Sign In failed, update UI appropriately
                // ...
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct, final MinorDetails userCredentials) {
        Log.d("mytag", "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            signInProgressBar.setVisibility(View.GONE);
                            signInProgressBarStatus.setText("Email verification Sucess");
                            Toast.makeText(getApplicationContext(),"Sucess",Toast.LENGTH_SHORT).show();

                            FirebaseUser user = firebaseAuth.getCurrentUser();

                            new MSharedPreferences(getApplicationContext()).setSharedPref("uniqueUserId",userCredentials.getUserUniqueId());
                            new MSharedPreferences(getApplicationContext()).setSharedPref("username",userCredentials.getUserName());
                            userCredentials.setFunctionCalledForOperation("registrationProcess");
                            new ServerSideTask(getApplicationContext(),userCredentials).execute();

                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            signInProgressBarStatus.setText("Enter your phonenumber");
                            userPhonenumber.setVisibility(View.VISIBLE);
                            signInButton.setVisibility(View.INVISIBLE);
                            verifyPhonenumberbtn.setVisibility(View.VISIBLE);



                        } else {
                            signInProgressBar.setVisibility(View.GONE);
                            // If sign in fails, display a message to the user.
                            Log.w("mytag", "signInWithCredential:failure", task.getException());
                        }

                        // ...
                    }
                });
    }



    private boolean validateUserRegisteration(String phoneNumber){
        if(phoneNumber.length()==10){
            return true;
        }else {
            return false;
        }

    }


    @Override
    protected void onStart() {
        super.onStart();

    }


    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mRegistrationBroadCast);

    }

    private BroadcastReceiver mRegistrationBroadCast=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra("registrationIntent").equals("success")){
                Log.w("mytag", "register broadcast called");

                Log.i("mytag","phone number is valid");
                Toast.makeText(getApplicationContext(),"Phone number is valid",Toast.LENGTH_LONG).show();
                Intent homeIntent=new Intent(RegisterActivity.this,tabactivity.class);
                homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(homeIntent);
            }
        }
    };



    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 11: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    private void PhoneNumberRegistration(final String userPhoneNumberHolder){

        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
//                "http://digitate-convention.000webhostapp.com/beatme/Competion.php",
                "http://192.168.1.111/betme/registration.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        mDatabaseResult=response;
                        Log.i("mytag","registration response"+mDatabaseResult);

                        Intent intent=new Intent("registrationIntent");
                        intent.putExtra("registrationIntent","success");

                        //response should be valid json
                        Gson gson=new Gson();
                        try {
                            Status status = gson.fromJson(mDatabaseResult, Status.class);
                            if (status.getStatusType().equals("registration") && status.getStatusCode().equals("success")) {
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                            }else if(status.getStatusType().equals("registration") && status.getStatusCode().equals("already")){
                                Toast.makeText(getApplicationContext(),"Already registered",Toast.LENGTH_LONG).show();
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                            }
                            Log.i("mytag","valid json");


                        }catch(JsonSyntaxException jse){
                            Log.i("mytag","invalid json");
                            Intent homeIntent=new Intent(RegisterActivity.this,tabactivity.class);
                            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(homeIntent);
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("mytag", "something went "+error);
                //save the phonenumber to sharedpreference and make a pending intent which app has to do this next time
                //as a todo list
                new MSharedPreferences(getApplicationContext()).setSharedPref("userPhoneNumberPendingIntent","yes");
                //this means userphonenumber is valid and saved to sharedpreference but not sent to database
                Intent homeIntent=new Intent(RegisterActivity.this,tabactivity.class);
                homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(homeIntent);
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                map.put("userPhoneNumber",userPhoneNumberHolder);

                return map;
            }


        };
        requestQueue.add(stringRequest);


    }






}
