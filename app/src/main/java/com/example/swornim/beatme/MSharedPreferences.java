package com.example.swornim.beatme;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Swornim on 4/5/2017.
 *
 * This is the class that handles all the flags untill the apps Data is deleted
 */

public class MSharedPreferences {

    private Context context;
    private SharedPreferences SHARED_PREFERENCE_NAME;



    public MSharedPreferences(Context context) {
        this.context = context;
        SHARED_PREFERENCE_NAME= PreferenceManager.getDefaultSharedPreferences(this.context);
    }

    public String getSharedPref(String key) {

        //if no such flag exist then it creates one
        key= SHARED_PREFERENCE_NAME.getString(key,"");
        return key;
    }

    public void setSharedPref(String key,String value){
        //update the Data
        SHARED_PREFERENCE_NAME.getString(key,value);
        SharedPreferences.Editor editor=SHARED_PREFERENCE_NAME.edit();
        editor.putString(key,value);
        editor.apply();

    }
}
