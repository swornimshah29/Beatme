package com.example.swornim.beatme;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.swornim.musicnap.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Swornim on 3/18/2017.
 */
public class Newsfeed extends ArrayAdapter<MinorDetails> {
    private  Context mContext;
    private List<MinorDetails> postObjects=new ArrayList<>();



    public Newsfeed(Context context, List<MinorDetails> postObjects) {
        super(context, R.layout.newsfeedtemplate,postObjects);
        mContext=context;
        this.postObjects=postObjects;
    }


    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View mView=convertView;
        if(convertView==null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            mView = layoutInflater.inflate(R.layout.newsfeedtemplate, parent, false);
        }

        ImageView leftTeamImage=(ImageView) mView.findViewById(R.id.leftImageName);
        ImageView rightTeamImage=(ImageView) mView.findViewById(R.id.rightImageName);

        TextView leftTeamName=(TextView) mView.findViewById(R.id.leftTeamName);
        TextView rightTeamName=(TextView) mView.findViewById(R.id.rightTeamName);
        TextView dateOfTheEvent=(TextView) mView.findViewById(R.id.dateOfEvents);

        leftTeamName.setText(postObjects.get(position).getLeftTeamName());
        rightTeamName.setText(postObjects.get(position).getRightTeamName());
        dateOfTheEvent.setText(postObjects.get(position).getDate());

        switch (postObjects.get(position).getLeftTeamName()) {
            case "Liverpool":
                leftTeamImage.setImageResource(R.drawable.liverpool);
                break;
            case "Manchester":
                leftTeamImage.setImageResource(R.drawable.manu);
                break;
            case "Sunderland":
                leftTeamImage.setImageResource(R.drawable.sunderland);
                break;
            case "SouthHampton":
                leftTeamImage.setImageResource(R.drawable.southhamton);
                break;

            case "ManchesterCity":
                leftTeamImage.setImageResource(R.drawable.mancity);
                break;

            case "Chealsea":
                leftTeamImage.setImageResource(R.drawable.chelsea);
                break;

        }

        switch (postObjects.get(position).getRightTeamName()) {
            case "Liverpool":
                rightTeamImage.setImageResource(R.drawable.liverpool);
                break;
            case "Manchester":
                rightTeamImage.setImageResource(R.drawable.manu);
                break;
            case "Sunderland":
                rightTeamImage.setImageResource(R.drawable.sunderland);
                break;
            case "SouthHampton":
                rightTeamImage.setImageResource(R.drawable.southhamton);
                break;

            case "ManchesterCity":
                rightTeamImage.setImageResource(R.drawable.mancity);
                break;

            case "Chealsea":
                rightTeamImage.setImageResource(R.drawable.chelsea);
                break;

        }



        leftTeamImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogBox(postObjects.get(position).getLeftTeamName(),postObjects.get(position).getRightTeamName(),position);
            }
        });

        rightTeamImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogBox(postObjects.get(position).getRightTeamName(),postObjects.get(position).getLeftTeamName(),position);
            }
        });
        return mView;
    }


    private void alertDialogBox(final String teamSelect,final String teamNotSelect,final int mposition){
        final AlertDialog.Builder popBox = new AlertDialog.Builder(mContext);
        popBox.setTitle("Wanna go with " +teamSelect +"?");
        popBox.setCancelable(false);
        popBox.setPositiveButton("Yup", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                MinorDetails minordetails=new MinorDetails();
                minordetails.setFunctionCalledForOperation("saveUserBeatEvents");
                minordetails.setUserName(new MSharedPreferences(getContext()).getSharedPref("userName"));
                minordetails.setUserUniqueId(new MSharedPreferences(getContext()).getSharedPref("uniqueUserId"));
                minordetails.setEventId(postObjects.get(mposition).getEventId());
                minordetails.setEventName(postObjects.get(mposition).getEventName());
                minordetails.setWhichTeamSelected(teamSelect);
                minordetails.setWhichTeamNotSelected(teamNotSelect);
                new ServerSideTask(getContext(),minordetails).execute();
                Toast.makeText(getContext(), "Processing...", Toast.LENGTH_LONG).show();
                dialogInterface.dismiss();
            }
        });

        popBox.setNegativeButton("Nope", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        popBox.show();

    }



}
