package com.example.swornim.beatme.Fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.swornim.beatme.MSharedPreferences;
import com.example.swornim.beatme.MinorDetails;
import com.example.swornim.beatme.ServerSideTask;
import com.example.swornim.beatme.notificationUserInterface;
import com.example.swornim.musicnap.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/*
 This class deals with the notification stuffss
 */

public class SideBarNotificationFragment extends Fragment  {

    private ArrayAdapter<MinorDetails> adapter;
    private ListView mListView;
    public List<MinorDetails> postObjects=new ArrayList<>();



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

       LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mBroadCastReceiver,new IntentFilter("MyStatusReportEvent"));
        View mView= inflater.inflate(R.layout.fragment_side_bar_notification,container,false);
        mListView=(ListView) mView.findViewById(R.id.fragment_side_bar_notificationListview);
        adapter=new notificationUserInterface(getActivity(),postObjects);
        mListView.setAdapter(adapter);

        MinorDetails minorDetails=new MinorDetails();
        minorDetails.setFunctionCalledForOperation("StatusReport");
        new ServerSideTask(getActivity(),minorDetails).execute();

        return mView;
    }

    private BroadcastReceiver mBroadCastReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            final String MyStatusReportEvent=new MSharedPreferences(getContext()).getSharedPref("MyStatusReportEvent");
            //process the json data and update the adapter for new  newsfeedtemplate
            try {
                JSONArray root = new JSONArray(MyStatusReportEvent);
                postObjects.clear();
                for(int i=0;i<root.length();i++){

                    JSONArray innerArray=root.getJSONArray(i);
                    //loop inside the loop
                    for(int i1=0;i1<innerArray.length();i1++){
                        JSONObject innerObject=innerArray.getJSONObject(i1);
                        //extract the first arrays datas
                        MinorDetails tempObject=new MinorDetails();

                        tempObject.setLooserName(innerObject.getString("looserName"));
                        tempObject.setWinnerName(innerObject.getString("winnerName"));
                        tempObject.setLooserScore(innerObject.getInt("looserScore"));
                        tempObject.setWinnerScore(innerObject.getInt("winnerScore"));
                        tempObject.setLeftTeamName(innerObject.getString("looserTeamName"));
                        tempObject.setRightTeamName(innerObject.getString("winnerTeamName"));
                        tempObject.setRightTeamName(innerObject.getString("winnerTeamName"));
                        tempObject.setStatusOfBeat(innerObject.getString("statusOfBeat"));
                        tempObject.setWhoWon(innerObject.getString("whoWon"));
                        tempObject.setWhoLost(innerObject.getString("whoLost"));
                        postObjects.add(tempObject);

                    }
                }
                adapter.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mBroadCastReceiver);
    }





}
