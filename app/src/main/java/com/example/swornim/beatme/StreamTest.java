package com.example.swornim.beatme;

import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.swornim.musicnap.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class StreamTest extends AppCompatActivity {
    private static String FIRST_TIME_INSERT;

    private Button button;
    String JSON="{\n" +
            "    \"epl\":\n" +
            "    [\n" +
            "        \n" +
            "        \n" +
            "    {\n" +
            "        \"eventsId\":\"f23r322r32\",\n" +
            "        \"leftTeamName\":\"manu\",\n" +
            "        \"rightTeamName\":\"chelsea\",\n" +
            "        \"meta\":\"Tommorow,9:00 \",\n" +
            "        \"questions\":\n" +
            "        [\n" +
            "            {\n" +
            "                \"question\":\"Will there be a penality?\",\n" +
            "                \"answer\":\"null\",\n" +
            "                \"type\":\"yn\"\n" +
            "            },\n" +
            "            {\n" +
            "              \"question\":\"Will a player get Red card ?\",\n" +
            "              \"answer\":\"null\",\n" +
            "                \"type\":\"yn\"\n" +
            "\n" +
            "            },\n" +
            "            {\n" +
            "              \"question\":\"How many MANU players will get yellow card ?\",\n" +
            "              \"answer\":\"null\",\n" +
            "                \"type\":\"mcq\"\n" +
            "                \n" +
            "            },\n" +
            "        \n" +
            "            \n" +
            "        ]\n" +
            "        \n" +
            "    },\n" +
            "        {\n" +
            "        \"eventsId\":\"2322f2\",\n" +
            "        \"leftTeamName\":\"southampton\",\n" +
            "        \"rightTeamName\":\"f23r322r32\",\n" +
            "        \"meta\":\"Tommorow,9:00 \",\n" +
            "            \"questions\":\n" +
            "        [\n" +
            "            {\n" +
            "                \"question\":\"Will there be a penality?\",\n" +
            "                \"answer\":\"null\",\n" +
            "                \"type\":\"yn\"\n" +
            "            },\n" +
            "            {\n" +
            "              \"question\":\"Will a player get Red card ?\",\n" +
            "              \"answer\":\"null\",\n" +
            "                \"type\":\"yn\"\n" +
            "\n" +
            "            },\n" +
            "            {\n" +
            "              \"question\":\"How many MANU players will get yellow card ?\",\n" +
            "              \"answer\":\"null\",\n" +
            "                \"type\":\"mcq\"\n" +
            "                \n" +
            "            },\n" +
            "        \n" +
            "            \n" +
            "        ]\n" +
            "        \n" +
            "    },\n" +
            "        {\n" +
            "        \"eventsId\":\"f23r322r32\",\n" +
            "        \"leftTeamName\":\"everton\",\n" +
            "        \"rightTeamName\":\"swansea\",\n" +
            "        \"meta\":\"Tommorow,9:00 \",\n" +
            "            \"questions\":\n" +
            "        [\n" +
            "            {\n" +
            "                \"question\":\"Will there be a penality?\",\n" +
            "                \"answer\":\"null\",\n" +
            "                \"type\":\"yn\"\n" +
            "            },\n" +
            "            {\n" +
            "              \"question\":\"Will a player get Red card ?\",\n" +
            "              \"answer\":\"null\",\n" +
            "                \"type\":\"yn\"\n" +
            "\n" +
            "            },\n" +
            "            {\n" +
            "              \"question\":\"How many MANU players will get yellow card ?\",\n" +
            "              \"answer\":\"null\",\n" +
            "                \"type\":\"mcq\"\n" +
            "                \n" +
            "            },\n" +
            "        \n" +
            "            \n" +
            "        ]\n" +
            "        \n" +
            "    }\n" +
            "        \n" +
            "        \n" +
            "    ]\n" +
            "    \n" +
            "}";
    private MediaPlayer mediaPlayer;
    private FirebaseStorage storage= FirebaseStorage.getInstance();
    private StorageReference storageReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stream_test);
        FIRST_TIME_INSERT=new MSharedPreferences(getApplicationContext()).getSharedPref("First_time_insert");


        button=(Button) findViewById(R.id.SongplayingId);

       final  Mdatabase mdatabase=new Mdatabase(getApplicationContext(),"Dev.db");


        if(FIRST_TIME_INSERT.equals("")) {
            Toast.makeText(getApplication(), "yup", Toast.LENGTH_SHORT).show();
            new MSharedPreferences(getApplicationContext()).setSharedPref("First_time_insert","already");
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Log.i("mytag",mdatabase.readAllContacts());
                Intent photoPickIntent = new Intent(Intent.ACTION_PICK);
                photoPickIntent.setType("image/*");
                startActivityForResult(photoPickIntent, 0);
            }
        });



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == RESULT_OK) {
            Uri targetUri = data.getData();

            String[] projection = {MediaStore.Images.Media.DATA,
                    MediaStore.Images.Media.DISPLAY_NAME,
                    MediaStore.Images.Media.DATE_ADDED
            };
            Cursor cursor = managedQuery(targetUri, projection, null, null, null);
            if (cursor != null) {
                int column_index = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                String path = cursor.getString(column_index);
                String imageName = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME));

                Log.i("mytag", path);//save this path for offline photourl in database
                //TODO save this path in database_android

                Log.i("mytag", imageName);
                Log.i("mytag", targetUri.toString());

                ImageView imageView = (ImageView) findViewById(R.id.photoSelectedId);

                Glide.with(getApplicationContext()).load(targetUri) //or use path
                        .centerCrop()
                        .crossFade()
                        .into(imageView);


            }
        }
    }

}


