package com.example.swornim.beatme;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.swornim.beatme.datastructure.Payment;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Swornim on 1/27/2017.
 */
public class MSQLiteDatabase extends SQLiteOpenHelper {

    private static Context CONTEXT;
    private Payment payment;
    private Map<String,Object> contactListMap=new HashMap<>();



    public MSQLiteDatabase(Context context, Payment payment) {
        super(context,"PAYMENT_CONFIG.db", null, 1);
        this.CONTEXT=context;
        this.payment=payment;

    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE PAYMENTS( ID INTEGER PRIMARY KEY AUTOINCREMENT,TOO TEXT , FROOM TEXT,AMOUNT TEXT, STATUS TEXT,SYNC TEXT ,PAYMENT_ID TEXT) ;");
        //TO AND FROM conflicts to keywords soo changing the name to TOO AND FROOM
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS PAYMENTS;");
        onCreate(db);
    }


    public String insertLoan(){
        Log.i("mytag", "INSERT CALLED");

         if(isPresent(payment.getPaymentId()).equals("false")){
            //insert the new loan
            ContentValues contentValues = new ContentValues();
            contentValues.put("TOO", payment.getTo());
            contentValues.put("FROOM", payment.getFrom());
            contentValues.put("AMOUNT", payment.getAmount());
            contentValues.put("SYNC", payment.getSync());
            contentValues.put("STATUS", payment.getPaymentStatus());
            contentValues.put("PAYMENT_ID", payment.getPaymentId());
            if (this.getWritableDatabase().insertOrThrow("PAYMENTS", null, contentValues) != -1)
                return "success";//success
            else
                return "failed";

        }
        return "present";
    }

    public String updateNSynchronize(){
        /*
        true means servers also has been update and repond came succesfully
        false means user is offline and saving to database
        status can be either payed or loan depends on the succes of the USSD
        */
         if(isPresent(payment.getPaymentId()).equals("true")){ //id present then do work
            ContentValues contentValues = new ContentValues();
            contentValues.put("STATUS", payment.getPaymentStatus());
            contentValues.put("SYNC", payment.getSync());

            if (this.getWritableDatabase().update("PAYMENTS", contentValues, "PAYMENT_ID=" + payment.getPaymentId(), null) != -1)
                return "success";
            else
                return "failed";

        }
        return "notPresent";//no such row may be deleted

    }


    private String isPresent(String paymentId){

        Cursor cursor=this.getReadableDatabase().rawQuery("SELECT * FROM PAYMENTS WHERE PAYMENT_ID='"+paymentId+"'",null);
            while (cursor.moveToNext()) {
                Log.i("mytag", "already inserted id " + cursor.getString(6));

                if (cursor.getString(6).equals(paymentId)){

                    cursor.close();
                    return "true";
                }

            }
            cursor.close();
            return "false";


    }

    public void readlAll(){
        Log.i("mytag", "read CALLED");

        Cursor cursor=this.getReadableDatabase().rawQuery("SELECT * FROM PAYMENTS",null);
        Log.i("mytag", "read CALLED cursor"+ cursor);

        if(cursor!=null){
            while (cursor.moveToNext()){
                Log.i("mytag","status: "+cursor.getString(4));
                Log.i("mytag","sync: "+cursor.getString(5));
                Log.i("mytag","paymentId: "+cursor.getString(6));
            }
        }


    }



}



