package com.example.swornim.beatme;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Swornim on 10/12/2017.
 */

public class Mdatabase extends SQLiteOpenHelper {


    private static Context CONTEXT;
    private Map<String,Object> contactListMap=new HashMap<>();



    public Mdatabase(Context context, String dbName) {
        super(context,dbName, null, 1);
        CONTEXT=context;

    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        //json storage inside JSON_STRING
        db.execSQL("CREATE TABLE SPONSER( ID INTEGER PRIMARY KEY AUTOINCREMENT,JSON_STRING TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS SPONSER;");
        onCreate(db);
    }


    public void insertUserContacts(String jsonString){
        ContentValues contentValues=new ContentValues();
        contentValues.put("JSON_STRING",jsonString);
        this.getWritableDatabase().insertOrThrow("SPONSER",null,contentValues);
    }

    public void DeleteUserContact(String name){
        this.getWritableDatabase().delete("SPONSER","NAME='"+name+"'",null);
    }

    public String readAllContacts(){
        Cursor cursor=this.getReadableDatabase().rawQuery("SELECT ID, JSON_STRING FROM SPONSER",null);
        String jsonString=null;
        while(cursor.moveToNext()){
             jsonString=cursor.getString(1);
            Log.i("mytag",cursor.getString(0));
        }
        cursor.close();//to free the cursor
        return jsonString;
    }
}
