package com.example.swornim.beatme;

import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.swornim.beatme.datastructure.Events;
import com.example.swornim.musicnap.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class test extends AppCompatActivity {

    private StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sidebarlayout);
        storageReference= FirebaseStorage.getInstance().getReference().child("sponsers_products/ff.jpg");
//        Log.i("mytag",storageReference.child("/sponsers_products");
//        uploadFileNow();


        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
//                "http://digitate-convention.000webhostapp.com/beatme/Competion.php",
                "http://192.168.1.111/betme/Events.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        String mResult = response;
                        Log.i("mytag",response);

                        Gson gson=new Gson();

                        com.example.swornim.beatme.datastructure.Events events= gson.fromJson(response, com.example.swornim.beatme.datastructure.Events.class);
                        Log.i("mytag","amount "+events.getEpl().get(0).getAmount());


//                                Intent intent = new Intent("notificationIntent");
//                                intent.putExtra("MyNbet", mResult);
//                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
//                                handler.removeCallbacks(runnable);

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
//                        Log.i("mytag", "Resending the data");
//                handler.postDelayed(runnable, 1500);
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<>();
//                        map.put("uniqueId", new MSharedPreferences(getApplicationContext()).getSharedPref("uniqueUserId"));
//                        map.put("Nbet", "yup");

                return map;
            }


        };
        requestQueue.add(stringRequest);


    }

    public void uploadFileNow() {
        String path= Environment.getExternalStorageDirectory().getAbsolutePath()+"/MusicNap/";
        File file=new File(path);
        File file1=new File(file,"ff.jpg");
        Uri fileTobeuploaded = Uri.fromFile(file1);

        if (file.exists())
            Log.i("mytag","yes file exists");
        else
            Log.i("mytag","File doesnot exit");

        UploadTask uploadTask = storageReference.putFile(fileTobeuploaded);//responsible for upload
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "Errror: " + e.toString(), Toast.LENGTH_LONG).show();

            }
        });

        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @SuppressWarnings("VisibleForTests")
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Log.i("mytag",taskSnapshot.getDownloadUrl().toString());

            }
        });

        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @SuppressWarnings("VisibleForTests")
            @Override
            public void onProgress(final UploadTask.TaskSnapshot taskSnapshot) {
                Log.i("mytag",String.valueOf(taskSnapshot.getBytesTransferred()));


            }
        });

    }


}
