package com.example.swornim.beatme;

/*This class is to hold the details about users and all kinds of activities flags*/

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MinorDetails implements Serializable{

    private String userName;
    private String userUniqueId;
    private String functionCalledForOperation;
    private String url;
    private String eventName;
    private String eventId;
    private String leftTeamName;
    private String rightTeamName;
    private String date;
    private String leftImageSelected;
    private String rightImageSelected;
    private String requestType;//login or register flag
    private String password;
    private String phoneNumber;
    private String whichTeamSelected;
    private String whichTeamNotSelected;
    private String follows;//fav icon of the user
    private String betLost;
    private String score;
    private String otherUserNames;
    private String userPhoneNumber;
    private String loanAmount;
    private String opponentNumber;
    private String opponentUserName;
    private List<String> arrays=new ArrayList<>();
    private List<MinorDetails> subQuestion=new ArrayList<>();

    //events_product_user
    String numberValue;
    String productUrl;




    //compete class details

    private String competitorsPoints;
    private String competitorsUserName;
    private String competitorsStatus;
    private String competitorsTeamIcon;
    private String TrophyNumber;



    public String getProductUrl() {
        return productUrl;
    }

    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }

    public String getNumberValue() {
        return numberValue;
    }

    public void setNumberValue(String numberValue) {
        this.numberValue = numberValue;
    }

    public List<MinorDetails> getSubQuestion() {
        return subQuestion;
    }

    public void setSubQuestion(List<MinorDetails> subQuestion) {
        this.subQuestion = subQuestion;
    }

    public List<String> getArrays() {
        return arrays;
    }

    public void setArrays(List<String> arrays) {
        this.arrays = arrays;
    }

    public String getOpponentUserName() {
        return opponentUserName;
    }

    public void setOpponentUserName(String opponentUserName) {
        this.opponentUserName = opponentUserName;
    }

    public String getOpponentNumber() {
        return opponentNumber;
    }

    public void setOpponentNumber(String opponentNumber) {
        this.opponentNumber = opponentNumber;
    }

    public String getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
    }

    public String getUserPhoneNumber() {
        return userPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber) {
        this.userPhoneNumber = userPhoneNumber;
    }

    public String getTrophyNumber() {
        return TrophyNumber;
    }

    public void setTrophyNumber(String trophyNumber) {
        TrophyNumber = trophyNumber;
    }

    public String getCompetitorsTeamIcon() {
        return competitorsTeamIcon;
    }

    public void setCompetitorsTeamIcon(String competitorsTeamIcon) {
        this.competitorsTeamIcon = competitorsTeamIcon;
    }

    public String getCompetitorsPoints() {
        return competitorsPoints;
    }

    public void setCompetitorsPoints(String competitorsPoints) {
        this.competitorsPoints = competitorsPoints;
    }

    public String getCompetitorsUserName() {
        return competitorsUserName;
    }

    public void setCompetitorsUserName(String competitorsUserName) {
        this.competitorsUserName = competitorsUserName;
    }

    public String getCompetitorsStatus() {
        return competitorsStatus;
    }

    public void setCompetitorsStatus(String competitorsStatus) {
        this.competitorsStatus = competitorsStatus;
    }

    public String getFollows() {
        return follows;
    }

    public void setFollows(String follows) {
        this.follows = follows;
    }

    public String getBetLost() {
        return betLost;
    }

    public void setBetLost(String betLost) {
        this.betLost = betLost;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getOtherUserNames() {
        return otherUserNames;
    }

    public void setOtherUserNames(String otherUserNames) {
        this.otherUserNames = otherUserNames;
    }


    //this is the variable associated with the notifications
    private String looserName;
    private String winnerName;
    private String looserTeamName;
    private String winnerTeamName;
    private int looserScore;
    private int winnerScore;
    private String statusOfBeat;
    private String whoWon;
    private String whoLost;


    //this is for the recharge card gifts
    private String rechargeCardNumber;
    private String recgargeCardType;
    private String recharCardValidation;



    //for newsfeed
    private String userName1;
    private String userName2;
    private String uniqueId1;
    private String uniqueId2;
    private String teamName1;
    private String teamName2;
    private String winby;
    private String lostBy;






    private List<String> jsonComments;

    public List<String> getJsonComments() {
        return jsonComments;
    }

    public void setJsonComments(List<String> jsonComments) {
        this.jsonComments = jsonComments;
    }

    public String getRechargeCardNumber() {
        return rechargeCardNumber;
    }

    public void setRechargeCardNumber(String rechargeCardNumber) {
        this.rechargeCardNumber = rechargeCardNumber;
    }

    public String getRecgargeCardType() {
        return recgargeCardType;
    }

    public void setRecgargeCardType(String recgargeCardType) {
        this.recgargeCardType = recgargeCardType;
    }

    public String getRecharCardValidation() {
        return recharCardValidation;
    }

    public void setRecharCardValidation(String recharCardValidation) {
        this.recharCardValidation = recharCardValidation;
    }

    public String getWhichTeamSelected() {
        return whichTeamSelected;
    }

    public void setWhichTeamSelected(String whichTeamSelected) {
        this.whichTeamSelected = whichTeamSelected;
    }

    public String getWhichTeamNotSelected() {
        return whichTeamNotSelected;
    }

    public void setWhichTeamNotSelected(String whichTeamNotSelected) {
        this.whichTeamNotSelected = whichTeamNotSelected;
    }

    public String getLeftImageSelected() {
        return leftImageSelected;
    }

    public void setLeftImageSelected(String leftImageSelected) {
        this.leftImageSelected = leftImageSelected;
    }

    public String getRightImageSelected() {
        return rightImageSelected;
    }

    public void setRightImageSelected(String rightImageSelected) {
        this.rightImageSelected = rightImageSelected;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getWhoWon() {
        return whoWon;
    }

    public void setWhoWon(String whoWon) {
        this.whoWon = whoWon;
    }

    public String getWhoLost() {
        return whoLost;
    }

    public void setWhoLost(String whoLost) {
        this.whoLost = whoLost;
    }

    public String getStatusOfBeat() {
        return statusOfBeat;
    }

    public void setStatusOfBeat(String statusOfBeat) {
        this.statusOfBeat = statusOfBeat;
    }

    public String getLooserName() {
        return looserName;
    }

    public void setLooserName(String looserName) {
        this.looserName = looserName;
    }

    public String getWinnerName() {
        return winnerName;
    }

    public void setWinnerName(String winnerName) {
        this.winnerName = winnerName;
    }

    public String getLooserTeamName() {
        return looserTeamName;
    }

    public void setLooserTeamName(String looserTeamName) {
        this.looserTeamName = looserTeamName;
    }

    public String getWinnerTeamName() {
        return winnerTeamName;
    }

    public void setWinnerTeamName(String winnerTeamName) {
        this.winnerTeamName = winnerTeamName;
    }

    public int getLooserScore() {
        return looserScore;
    }

    public void setLooserScore(int looserScore) {
        this.looserScore = looserScore;
    }

    public int getWinnerScore() {
        return winnerScore;
    }

    public void setWinnerScore(int winnerScore) {
        this.winnerScore = winnerScore;
    }


    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getLeftTeamName() {
        return leftTeamName;
    }

    public void setLeftTeamName(String leftTeamName) {
        this.leftTeamName = leftTeamName;
    }

    public String getRightTeamName() {
        return rightTeamName;
    }

    public void setRightTeamName(String rightTeamName) {
        this.rightTeamName = rightTeamName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUserUniqueId() {
        return userUniqueId;
    }

    public void setUserUniqueId(String userUniqueId) {
        this.userUniqueId = userUniqueId;
    }

    public String getFunctionCalledForOperation() {
        return functionCalledForOperation;
    }

    public void setFunctionCalledForOperation(String functionCalledForOperation) {
        this.functionCalledForOperation = functionCalledForOperation;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getUserName1() {
        return userName1;
    }

    public void setUserName1(String userName1) {
        this.userName1 = userName1;
    }

    public String getUserName2() {
        return userName2;
    }

    public void setUserName2(String userName2) {
        this.userName2 = userName2;
    }

    public String getUniqueId1() {
        return uniqueId1;
    }

    public void setUniqueId1(String uniqueId1) {
        this.uniqueId1 = uniqueId1;
    }

    public String getUniqueId2() {
        return uniqueId2;
    }

    public void setUniqueId2(String uniqueId2) {
        this.uniqueId2 = uniqueId2;
    }

    public String getTeamName1() {
        return teamName1;
    }

    public void setTeamName1(String teamName1) {
        this.teamName1 = teamName1;
    }

    public String getTeamName2() {
        return teamName2;
    }

    public void setTeamName2(String teamName2) {
        this.teamName2 = teamName2;
    }

    public String getWinby() {
        return winby;
    }

    public void setWinby(String winby) {
        this.winby = winby;
    }

    public String getLostBy() {
        return lostBy;
    }

    public void setLostBy(String lostBy) {
        this.lostBy = lostBy;
    }
}
