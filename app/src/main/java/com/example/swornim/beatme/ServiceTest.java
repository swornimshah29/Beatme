package com.example.swornim.beatme;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.swornim.musicnap.R;

public class ServiceTest extends AppCompatActivity implements View.OnClickListener {

    private Button buttonStart,buttonStop,generateNumber,bindService;
    private TextView showNumber;
    private Intent serviceIntent;
    private MService mService;
    private ServiceConnection serviceConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_test);

        buttonStart=(Button) findViewById(R.id.start);
        buttonStop=(Button) findViewById(R.id.stop);
        bindService=(Button) findViewById(R.id.bind);
        generateNumber=(Button) findViewById(R.id.generate);
        showNumber=(TextView) findViewById(R.id.showNumber);

        buttonStart.setOnClickListener(this);
        buttonStop.setOnClickListener(this);
        bindService.setOnClickListener(this);
        generateNumber.setOnClickListener(this);
        Log.i("mytag","Main thread id"+Thread.currentThread().getId());

//        serviceIntent=new Intent(getApplicationContext(),MService.class);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.start:
//                serviceConnection=new ServiceConnection() {
//                    @Override
//                    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
//                        MService.MServiceBinder mServiceBinder=(MService.MServiceBinder)iBinder;
//                        mService=mServiceBinder.getService();
//
//                    }
//
//                    @Override
//                    public void onServiceDisconnected(ComponentName componentName) {
//
//                    }
//                };
//                bindService(serviceIntent,serviceConnection, Context.BIND_AUTO_CREATE);
                Intent intent=new Intent(getApplicationContext(),MService.class);
                intent.setAction("stop");
                startService(intent);

//                new UploadImage(getApplicationContext()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;


            case R.id.generate:
                Intent intent1=new Intent(getApplicationContext(),MService.class);
                stopService(intent1);


        }
    }






}
