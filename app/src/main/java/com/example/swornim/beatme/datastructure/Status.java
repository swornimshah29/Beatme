package com.example.swornim.beatme.datastructure;

public class Status {
    /*This class represents all the status code from the server reponse
    whether the operation has been success or not
     */
    private String statusType;
    private String statusCode;


    public String getStatusType() {
        return statusType;
    }

    public void setStatusType(String statusType) {
        this.statusType = statusType;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
