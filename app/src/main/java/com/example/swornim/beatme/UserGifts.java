package com.example.swornim.beatme;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.swornim.musicnap.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class UserGifts extends AppCompatActivity {

    private ListView usergiftsListView;
    private ArrayAdapter<MinorDetails> adapter;
    private ImageView imageView;
    private ImageView uploaderCloudImage;
    private StorageReference storageReference;
    private List<MinorDetails> postObjects=new ArrayList<>();//should always be intialized even Data is null


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_gifts);
        imageView=(ImageView) findViewById(R.id.displayUploadingPhoto);
        uploaderCloudImage=(ImageView) findViewById(R.id.uploaderCloudImage);

        if(getIntent()!=null){
            Log.i("mytag","Data "+ getIntent().getStringExtra("url"));
            Glide.with(getApplicationContext()).load(getIntent().getStringExtra("url")) //or use path
                        .crossFade()
                        .into(imageView);
        }

//        usergiftsListView=(ListView) findViewById(R.id.usergiftsListView);
//        adapter=new UserGiftsAdapter(getApplicationContext(),postObjects);
//        usergiftsListView.setAdapter(adapter);
//
//        MinorDetails minorDetails=new MinorDetails();
//        minorDetails.setFunctionCalledForOperation("LoadGifts");
//        new ServerSideTask(getApplicationContext(),minorDetails).execute();

       // getSupportActionBar().setTitle("Gifts");

        uploaderCloudImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                storageReference= FirebaseStorage.getInstance().getReference().child("sponsers_products/"+getIntent().getStringExtra("imageName"));
//                UploadTask uploadTask=storageReference.putFile(Uri.parse(getIntent().getStringExtra("url")));
//                uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
//                    @Override
//                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
//                        Log.i("mytag","Data "+ task.getResult().getDownloadUrl());
//
//                        Toast.makeText(getApplicationContext(),"download "+task.getResult().getDownloadUrl(),Toast.LENGTH_SHORT).show();
//                    }
//                });
                Intent intent=new Intent(getApplicationContext(),MService.class);
                intent.putExtra("url",getIntent().getStringExtra("url"));
                intent.putExtra("imageName",getIntent().getStringExtra("imageName"));
                startService(intent);

            }
        });



    }


    @Override
    protected void onStart() {
        super.onStart();

//        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mBroadCastGiftsReceiver,new IntentFilter("giftsDetails"));

    }

    @Override
    protected void onStop() {
        super.onStop();
//        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mBroadCastGiftsReceiver);
    }

    private BroadcastReceiver mBroadCastGiftsReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String giftsDetails=new MSharedPreferences(getApplicationContext()).getSharedPref("giftsDetails");
            postObjects.clear();
            //process the json Data and update the adapter for new  newsfeedtemplate
            try {
                JSONArray root = new JSONArray(giftsDetails);
                for(int i=0;i<root.length();i++){
                    JSONArray innerArray=root.getJSONArray(i);
                    //loop inside the loop
                    for(int i1=0;i1<innerArray.length();i1++){
                        JSONObject innerObject=innerArray.getJSONObject(i1);
                        //extract the first arrays datas
                        MinorDetails tempObject=new MinorDetails();
                        tempObject.setRechargeCardNumber(innerObject.getString("cardNumber"));
                        tempObject.setRecgargeCardType(innerObject.getString("cardType"));
                        tempObject.setRecharCardValidation(innerObject.getString("validation"));
                        postObjects.add(tempObject);
                    }
                }
                adapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };


}
